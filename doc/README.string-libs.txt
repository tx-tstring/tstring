Quick overview of some string libs
==================================
Frank Bergmann <http://www.tuxad.com>
1.0, 03 November 2018

This article gives a quick overview about four C string libraries.


glib
----

struct
~~~~~~

[source,c]
----
struct GString {
  gchar  *str;
  gsize len;
  gsize allocated_len;
};
----

Pros
~~~~

  . simple struct
  . automated growth
  . binary data capable

Cons
~~~~

  . gsize used in struct is platform dependent
  . buffer not part of struct, just buffer pointer
  . many and bloated functions, not targeted towards size
  . return values not compatible with functions from string.h


Better String Library BString
-----------------------------

struct
~~~~~~

[source,c]
----
struct tagbstring {
	int mlen;
	int slen;
	unsigned char * data;
};
----

Pros
~~~~

  . simple struct
  . automated growth
  . binary data capable
  . unsigned char provokes compiler warnings on implicit casts
  . (stream support)

Cons
~~~~

  . int used in struct is platform dependent
  . buffer not part of struct, just buffer pointer
  . return values not compatible with functions from string.h


lockless dynamic cstrings
-------------------------

struct
~~~~~~

[source,c]
----
typedef struct string string;
struct string
{
	char *s;
	size_t size;
	size_t b_size;
};
#define STR_FREEABLE (1ULL << 63)
static void strrealloc(string *s)
{
	/* Can't realloc? */
	if (!(s->b_size & STR_FREEABLE)) return;
	...
}
----

Pros
~~~~

  . simple struct
  . automated growth
  . binary data capable
  . dynamic and static allocation

Cons
~~~~

  . size_t used in struct is platform dependent
  . buffer not part of struct, just buffer pointer
  . return values not compatible with functions from string.h


SDS
---

struct
~~~~~~

[source,c]
----
typedef char *sds;

/* Note: sdshdr5 is never used, we just access the flags byte directly.
 * However is here to document the layout of type 5 SDS strings. */
struct __attribute__ ((__packed__)) sdshdr5 {
    unsigned char flags; /* 3 lsb of type, and 5 msb of string length */
    char buf[];
};
struct __attribute__ ((__packed__)) sdshdr8 {
    uint8_t len; /* used */
    uint8_t alloc; /* excluding the header and null terminator */
    unsigned char flags; /* 3 lsb of type, 5 unused bits */
    char buf[];
};
struct __attribute__ ((__packed__)) sdshdr16 {
    uint16_t len; /* used */
    uint16_t alloc; /* excluding the header and null terminator */
    unsigned char flags; /* 3 lsb of type, 5 unused bits */
    char buf[];
};
struct __attribute__ ((__packed__)) sdshdr32 {
    uint32_t len; /* used */
    uint32_t alloc; /* excluding the header and null terminator */
    unsigned char flags; /* 3 lsb of type, 5 unused bits */
    char buf[];
};
struct __attribute__ ((__packed__)) sdshdr64 {
    uint64_t len; /* used */
    uint64_t alloc; /* excluding the header and null terminator */
    unsigned char flags; /* 3 lsb of type, 5 unused bits */
    char buf[];
};
----

Pros
~~~~

  . buffer is part of struct
  . automated growth
  . binary data capable
  . return values compatible with functions from string.h

Cons
~~~~

  . header is prefixed before buffer, no type safety
  . no alignment


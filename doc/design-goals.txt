Design goals of C string library "tstring"
==========================================
Frank Bergmann <http://www.tuxad.com>
1.0, 23 December 2018

This article gives a quick overview about four C string libraries.


Existing C string libs
----------------------

Four well known C string libs are roughly compared in doc/README.string-libs
article.
Not mentioned in this article are the brilliant libs ustr and vstr by James
Antill (www.and.org). His comparison of C string libs was done 11 years ago
and thus it is not up-to-date but it's still worth reading:
http://www.and.org/vstr/comparison

Design goals
------------

  . tstring should have ONE simple struct
  . alignments in struct
  . efficient for small AND big strings
  . types used should be platform independent (C99)
  . automatic growth
  . binary data capable (this does NOT mean full UTF-8 support)
  . read-only strings capable
  . stream support is optional
  . like e.g. SDS it should be "return value compatible" with C str
  . it should be possible to use data storage within the struct itself
  . like e.g. lockless dynamic cstrings it should also support data pointer
    (static and dynamic allocation)
  . enhanced features like ref counts will be offered by bigger "txstring" lib
  . test coverage should be almost complete

Well, you see... that's almost impossible. ;-)


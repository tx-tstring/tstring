/**
 * Unit test for tstr_str().
 */
static void test_tstr_str() {
  char buf1[30];
  tstring ts1;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "1234567", 7);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(\"1234567\") != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(\"1234567\") returns same string");
  sput_fail_unless(tstr_len(ts_new) == 7, "tstr_cpycn(\"1234567\") returns tstring w len 7");

  sput_fail_unless(tstr_str(ts1, "1") == ts1, "tstr_str(\"1\") == tstring");
  sput_fail_unless(tstr_str(ts1, "123") == ts1, "tstr_str(\"123\") == tstring");
  sput_fail_unless(tstr_str(ts1, "23") == ts1+1, "tstr_str(\"23\") == tstring+1");
  sput_fail_unless(tstr_str(ts1, "24") == 0, "tstr_str(\"24\") == 0");
  sput_fail_unless(tstr_str(ts1, "567") == ts1+4, "tstr_str(\"567\") == tstring+4");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

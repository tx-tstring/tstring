/**
 * Unit test for tstr_tstr().
 */
static void test_tstr_tstr() {
  char buf1[30];
  char buf2[30];
  tstring ts1;
  tstring ts_tmp;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);
  ts_tmp = tstr_buffer2tstring(buf2, sizeof(buf2), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "123\000567\000", 8);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(\"123\\0567\\0\") != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(\"123\\0567\\0\") returns same string");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_cpycn(\"123\\0567\\0\") returns tstring w len 8");

  ts_new = tstr_cpycn(ts_tmp, "1", 1);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1, "tstr_tstr(\"1\") == tstring");
  ts_new = tstr_cpycn(ts_tmp, "123", 3);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1, "tstr_tstr(\"123\") == tstring");
  ts_new = tstr_cpycn(ts_tmp, "567", 3);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1+4, "tstr_tstr(\"567\") == tstring+4");
  ts_new = tstr_cpycn(ts_tmp, "23\000", 3);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1+1, "tstr_tstr(\"23\\0\") == tstring+1");
  ts_new = tstr_cpycn(ts_tmp, "24", 2);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == 0, "tstr_tstr(\"24\") == 0");
  ts_new = tstr_cpycn(ts_tmp, "3\0005", 3);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1+2, "tstr_tstr(\"3\\05\") == tstring+2");
  ts_new = tstr_cpycn(ts_tmp, "\00056", 3);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1+3, "tstr_tstr(\"\\056\") == tstring+3");
  ts_new = tstr_cpycn(ts_tmp, "7\000", 2);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == ts1+6, "tstr_tstr(\"7\\0\") == tstring+6");

  ts_new = tstr_cpycn(ts_tmp, "\000", 0);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == 0, "tstr_tstr(\"\") == 0");
  ts_new = tstr_cpycn(ts_tmp, "1234567890", 10);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == 0, "tstr_tstr(\"1234567890\") == 0");
  ts_new = tstr_cpycn(ts1, "", 0);
  sput_fail_unless(tstr_len(ts_new) == 0, "tstr_cpycn(\"\") returns tstring w len 0");
  ts_new = tstr_cpycn(ts_tmp, "1", 1);
  sput_fail_unless(tstr_tstr(ts1, ts_tmp) == 0, "tstr_tstr(\"1\") on empty tstring returns 0");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#include <unistd.h>

#define TSTR_NEW_SIZE1 63
#define TSTR_NEW_SIZE2 64
#define TSTR_NEW_SIZE3 65529
#define TSTR_NEW_SIZE4 65530
#define TSTR_NEW_SIZE5 524288

/**
 * Unit test for test_tstr_new(), tstr_size() and tstr_free().
 */
static void test_tstr_new() {

  /* initializing */
  tstring ts1 = tstr_new(TSTR_NEW_SIZE1);
  tstring ts2 = tstr_new(TSTR_NEW_SIZE2);
  tstring ts3 = tstr_new(TSTR_NEW_SIZE3);
  tstring ts4 = tstr_new(TSTR_NEW_SIZE4);
  tstring ts5 = tstr_new(TSTR_NEW_SIZE5);

  /* tests */

  sput_fail_unless(tstr_new(0) != 0, "size==0 returns NOT 0"); // must be tstring with size tstr_minimum_size
  sput_fail_unless(ts1 != 0, "tstr_new(63) created");
  sput_fail_unless(ts2 != 0, "tstr_new(64) created");
  sput_fail_unless(ts3 != 0, "tstr_new(65529) created");
  sput_fail_unless(ts4 != 0, "tstr_new(65530) created");
  sput_fail_unless(TSTR_IS_FREEABLE(ts1) != 0, "tstr_new(63) is freeable");

#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts1) == TSTR_HEADERTYPE_6, "type for tstr_new(63) is header6");
  sput_fail_unless(TSTR_TYPE(ts2) == TSTR_HEADERTYPE_16, "type for tstr_new(64) is header16");
  sput_fail_unless(TSTR_TYPE(ts3) == TSTR_HEADERTYPE_16, "type for tstr_new(65529) is header16");
#endif
  sput_fail_unless(TSTR_TYPE(ts4) == TSTR_HEADERTYPE_32, "type for tstr_new(65530) is header32");
#endif

  sput_fail_unless(tstr_size(ts1) == TSTR_NEW_SIZE1, "tstr_new(63) size matches");
  sput_fail_unless(tstr_size(ts2) == TSTR_NEW_SIZE2, "tstr_new(64) size matches");
  sput_fail_unless(tstr_size(ts3) == TSTR_NEW_SIZE3, "tstr_new(65529) size matches");
  sput_fail_unless(tstr_size(ts4) == TSTR_NEW_SIZE4, "tstr_new(65530) size matches");

  sput_fail_unless(ts5 != 0, "tstr_new(524288) created");
  sput_fail_unless(tstr_size(ts5) == TSTR_NEW_SIZE5, "tstr_new(524288) size matches");
  tstr_free(ts5);
#ifdef __THIS_SHOULD_SEGFAULT
  sput_fail_unless(tstr_size(ts5) == 0, "tstr_new(524288) size after tstr_free() is 0");
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

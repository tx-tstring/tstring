/* assume tstr_minimum_size to be less than this */
#define TSTR_DUP_STRING1 "123456789012345678901234567890123456789012345678901234567890"
#define TSTR_DUP_SIZE1 1000

/**
 * Unit test for tstr_dup().
 */
static void test_tstr_dup() {
  tstring ts1;
  tstring ts_binary;
  tstring ts_new;
  tstring ts_dup;

  /* initializing */
  char buf1[TSTR_DUP_SIZE1];
  char buf2[TSTR_DUP_SIZE1];
  ts1 = tstr_buffer2tstring(buf1, TSTR_DUP_SIZE1, 0);
  ts1 = tstr_cpyc(ts1, TSTR_DUP_STRING1);
  sput_fail_unless(tstr_len(ts1) == strlen(TSTR_DUP_STRING1), "init: ts1 has wrong len");
  ts_binary = tstr_buffer2tstring(buf2, TSTR_DUP_SIZE1, 0);
  ts_binary = tstr_cpyc(ts_binary, "123@456");
  sput_fail_unless(tstr_len(ts_binary) == 7, "init: ts_binary has wrong len");

  /* tests */

  /* invalid argument */
  sput_fail_unless(tstr_dup(0) == 0, "tstr_dup(0) returns 0");

  /* empty string */
  ts_new = tstr_new(0);
  ts_dup = tstr_dup(ts_new);
  sput_fail_unless(ts_dup != 0, "tstr_dup(tstr_new(0)) does not return 0");
  sput_fail_unless(tstr_len(ts_dup) == 0, "tstr_len(tstr_dup(tstr_new(0))) returns 0");
  sput_fail_unless(tstr_size(ts_dup) == tstr_minimum_size, "tstr_size(tstr_dup(tstr_new(0))) returns tstr_minimum_size");
  sput_fail_unless(ts_dup != ts_new, "tstr_dup(tstr_new(0)): pointers of original and duplicated tstring are different");
  sput_fail_unless(strcmp(ts_dup,ts_new) == 0, "tstr_dup(tstr_new(0)): same content");

  /* string with one char */
  ts_new = tstr_catc(ts_new, "1");
  ts_dup = tstr_dup(ts_new);
  sput_fail_unless(ts_dup != 0, "tstr_dup(one char) does not return 0");
  sput_fail_unless(tstr_len(ts_dup) == 1, "tstr_len(tstr_dup(one char)) returns 1");
  sput_fail_unless(tstr_size(ts_dup) == tstr_minimum_size, "tstr_size(tstr_dup(one char)) returns tstr_minimum_size");
  sput_fail_unless(ts_dup != ts_new, "tstr_dup(one char): pointers of original and duplicated tstring are different");
  sput_fail_unless(strcmp(ts_dup,ts_new) == 0, "tstr_dup(one char): same content");

  /* string greater than tstr_minimum_size */
  ts_dup = tstr_dup(ts1);
  sput_fail_unless(ts_dup != 0, "tstr_dup(string w/ normal len) does not return 0");
  sput_fail_unless(tstr_len(ts_dup) == strlen(TSTR_DUP_STRING1), "tstr_len(tstr_dup(string w/ normal len)) returns correct len");
  sput_fail_unless(tstr_size(ts_dup) != tstr_minimum_size, "tstr_size(tstr_dup(string w/ normal len)) does not tstr_minimum_size");
  sput_fail_unless(ts_dup != ts_new, "tstr_dup(string w/ normal len): pointers of original and duplicated tstring are different");
  sput_fail_unless(strcmp(ts_dup,ts1) == 0, "tstr_dup(string w/ normal len): same content");

  /* binary content */
  ((char *)ts_binary)[3] = '\0';
  ts_dup = tstr_dup(ts_binary);
  sput_fail_unless(ts_dup != 0, "tstr_dup(binary data) does not return 0");
  sput_fail_unless(tstr_len(ts_dup) == 7, "tstr_len(tstr_dup(binary data)) returns 7");
  sput_fail_unless(ts_dup != ts_new, "tstr_dup(binary data): pointers of original and duplicated tstring are different");
  sput_fail_unless(memcmp(ts_dup,ts_binary,7) == 0, "tstr_dup(binary data): same content");
  sput_fail_unless(((char *)ts_dup)[3] == 0, "tstr_dup(binary data): 0 within content");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

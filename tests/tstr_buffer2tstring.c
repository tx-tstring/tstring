#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
// for tstr_debugprinttstring()
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <unistd.h>

#define TSTR_BUFFER2TSTRING_SIZE1 65
#define TSTR_BUFFER2TSTRING_SIZE2 66
#define TSTR_BUFFER2TSTRING_SIZE3 65535
#define TSTR_BUFFER2TSTRING_SIZE4 65536
#define TSTR_BUFFER2TSTRING_SIZE5 64

/**
 * Unit test for tstr_init(), tstr_init_static(), tstr_buffer2tstring() and tstr_len().
 */
static void test_tstr_buffer2tstring() {
  uint16_t ttype;
  char buf8[TSTR_BUFFER2TSTRING_SIZE4];

  /* initializing */
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  char buf1[TSTR_BUFFER2TSTRING_SIZE1];
  tstring ts1 = tstr_buffer2tstring(buf1, TSTR_BUFFER2TSTRING_SIZE1, 0);
  char *buf2 = malloc(TSTR_BUFFER2TSTRING_SIZE2);
  tstring ts2 = tstr_buffer2tstring(buf2, TSTR_BUFFER2TSTRING_SIZE2, 1);
  char *buf3 = malloc(TSTR_BUFFER2TSTRING_SIZE3);
  tstring ts3 = tstr_buffer2tstring(buf3, TSTR_BUFFER2TSTRING_SIZE3, 1);
#endif
  char *buf4 = malloc(TSTR_BUFFER2TSTRING_SIZE4);
  tstring ts4 = tstr_buffer2tstring(buf4, TSTR_BUFFER2TSTRING_SIZE4, 1);
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  char *buf5 = malloc((uint64_t)UINT32_MAX + TSTR_BUFFER2TSTRING_SIZE5);
  tstring ts5 = tstr_buffer2tstring(buf5, (uint64_t)UINT32_MAX + TSTR_BUFFER2TSTRING_SIZE5, 1);
  char *buf6 = malloc((uint64_t)INT64_MAX);
  tstring ts6 = tstr_buffer2tstring(buf6, (uint64_t)INT64_MAX, 1);
#endif
  char *buf7 = malloc(TSTR_BUFFER2TSTRING_SIZE4);
  tstring ts7 = tstr_init(buf7, TSTR_BUFFER2TSTRING_SIZE4);
  tstring ts8 = tstr_init_static(buf8, TSTR_BUFFER2TSTRING_SIZE4);

  /* tests */

  /* invalid arguments */
  sput_fail_unless(tstr_buffer2tstring(0, TSTR_BUFFER2TSTRING_SIZE1, 0) == 0, "buf==0 returns 0");
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(tstr_buffer2tstring(buf1, 3, 0) == 0, "size<4 returns 0");
  ts1 = tstr_buffer2tstring(buf1, TSTR_BUFFER2TSTRING_SIZE1, 0);
#endif

  /* basic check */
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  sput_fail_unless(sizeof(size_t) >= 8, "sizeof(size_t) must be >= 8 for testing > 4 GiB\n***\n***\n***\n*** If this test did fail then please disabled/undefine TSTR_ALLOW_EXABYTE_STRINGS!\n***\n***\n***\n");
#endif

  /* conversion */
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 != 0, "buf[65] conversion successful");
  sput_fail_unless(tstr_len(ts1) == 0, "converted buf[65] len is 0");
  sput_fail_unless(ts2 != 0, "buf[66] conversion successful");
  sput_fail_unless(ts3 != 0, "buf[65535] conversion successful");
#endif
  sput_fail_unless(ts7 != 0, "buf[65536] tstr_init() successful");
  sput_fail_unless(ts8 != 0, "buf[65536] tstr_init_static() successful");
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  sput_fail_unless(ts5 != 0, "buf[UINT32_MAX+64] conversion successful");
  if (buf6) {
    sput_fail_unless(ts6 != 0, "buf[INT64_MAX] conversion successful");
  } else {
    printf("WARNING: malloc(INT64_MAX) failed, tstr_buffer2tstring() not tested for 8 EiB\n");
  }
#endif

  /* type */
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  ttype = TSTR_TYPE(ts4);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_32, "type for buf[65536] is header32");
#else
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  ttype = TSTR_TYPE(ts1);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_6, "type for buf[65] is header6");
  ttype = TSTR_TYPE(ts2);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_16, "type for buf[66] is header16");
  ttype = TSTR_TYPE(ts3);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_16, "type for buf[65535] is header16");
#endif
#ifndef _TSTR_SINGLE_STRUCT_USAGE
  ttype = TSTR_TYPE(ts4);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_32, "type for buf[65536] is header32");
#endif
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  ttype = TSTR_TYPE(ts5);
  sput_fail_unless(ttype == TSTR_HEADERTYPE_64, "type for buf[UINT32_MAX+64] is header64");
#endif
#endif

  /* is freeable? */
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_IS_FREEABLE(ts1) == 0, "buf[65] is not freeable");
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts4) != 0, "buf[65536] is freeable");
  sput_fail_unless(tstr_len(ts4) == 0, "converted buf[65536] len is 0");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define TSTR_TSTFREE_STRING1 "123456789012345678901234567890123456789012345678901234567890"
#define TSTR_TSTFREE_SIZE1 100

/**
 * Unit test for tstr_free().
 */
static void test_tstr_free() {
  tstring ts1;
  tstring ts2;
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  register char *p;
  size_t tsize;
#endif
  size_t c;


  /* initializing */
  char buf1[TSTR_TSTFREE_SIZE1];
  for (c=0;c<TSTR_TSTFREE_SIZE1;c++) buf1[c] = 'z';

  /* tests */

  ts1 = tstr_buffer2tstring(buf1, TSTR_TSTFREE_SIZE1, 0);
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  printf("_TSTR_USE_SECURE_ERASE_ON_INIT is enabled, checking memory after init\n");
  tsize = tstr_size(ts1);
  p = (char *) ts1;
  sput_fail_unless(p[0] == 0, "content 0 after init");
  for (c=0;c<tsize;c++) {
    if (p[c] != 0) {
      printf("p[c]=%d, c=%lu\n", p[c], c);
      sput_fail_unless(p[c] == 0, "content 0 after init");
    }
  }
  for (c=0;c<tsize;c++) {
    p[c] = 'z';
  }
#endif
  ts1 = tstr_cpyc(ts1, TSTR_TSTFREE_STRING1);
  if (tstr_len(ts1) != strlen(TSTR_TSTFREE_STRING1)) {
    sput_fail_unless(tstr_len(ts1) == strlen(TSTR_TSTFREE_STRING1), "init: ts1 has wrong len");
  }
  tstr_free(ts1);
  sput_fail_unless(tstr_len(ts1) == 0, "len 0 after free");
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
  printf("_TSTR_USE_SECURE_ERASE_ON_FREE is enabled, checking memory after tstr_free()\n");
  sput_fail_unless(buf1[0] == 0, "content 0 after free");
  for (c=0;c<TSTR_TSTFREE_SIZE1;c++) {
    if (buf1[c] != 0) {
      printf("buf1[c]=%d, c=%lu\n", buf1[c], c);
      sput_fail_unless(buf1[c] == 0, "content 0 after free");
    }
  }
#endif
  ts2 = tstr_cpyc(ts1, TSTR_TSTFREE_STRING1);
  sput_fail_unless(ts2 != ts1, "use after free causes new tstring");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

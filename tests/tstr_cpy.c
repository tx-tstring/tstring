// #define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
// // for tstr_debugprinttstring()
// #include <tstring-private.h>
// #undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

#define TSTR_CPYT_STRING1 "1"
#define TSTR_CPYT_STRING2 "123456789"
#define TSTR_CPYT_STRING3 "123456789012345678901234567890123456789012345678901234567890"
#define TSTR_CPYT_SIZE1 44
#define TSTR_CPYT_SIZE2 65530

/**
 * Unit test for tstr_cpy() tstr_resize() and tstr_len().
 */
static void test_tstr_cpy() {
  char sbuf[70000];
  tstring ts1;
  tstring ts2;
  tstring ts_new;
  tstring s;

  /* initializing */
  char buf1[TSTR_CPYT_SIZE1];
  ts1 = tstr_buffer2tstring(buf1, TSTR_CPYT_SIZE1, 0);
  char *buf2 = malloc(TSTR_CPYT_SIZE2);
  ts2 = tstr_buffer2tstring(buf2, TSTR_CPYT_SIZE2, 1);
  s = tstr_buffer2tstring(sbuf, 70000, 0);

  /* tests */

  /* invalid arguments */
  sput_fail_unless(tstr_cpy(0, s) == 0, "tstr_cpy(0,...) returns 0");
  sput_fail_unless(tstr_cpy(ts1, 0) == 0, "tstr_cpy(...,0) returns 0");

  /* tstr_resize() */
  ts_new = tstr_resize(0, TSTR_CPYT_SIZE1);
  sput_fail_unless(ts_new == 0, "tstr_resize() does not return 0 if tstring argument is 0");
  ts_new = tstr_resize(ts1, TSTR_CPYT_SIZE1);
  sput_fail_unless(ts_new != ts1, "tstr_resize() of tstring with same size does not return the tstring itself");
  ts_new = tstr_resize(ts1, 0);
  sput_fail_unless(tstr_size(ts_new) != tstr_len(ts1), "tstr_resize() with newsize 0 does not return a tstring with size=len");
  ts_new = tstr_resize(ts1, tstr_len(ts1)-1);
  sput_fail_unless(ts_new == 0, "tstr_resize() with newsize too low does not return 0");
  ts_new = tstr_resize(ts1, TSTR_CPYT_SIZE2);
  sput_fail_unless(tstr_size(ts_new) == TSTR_CPYT_SIZE2, "tstr_resize() does not return tstring with given size");

  strcpy(s, TSTR_CPYT_STRING2);
  //tstr_debugprinttstring(s);
  tstr_setlen(s, strlen(TSTR_CPYT_STRING2));
  ts_new = tstr_cpy(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "cpy to empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,s) == 0, "cpy to empty tstring, content match");
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) == 0, "tstring is not freeable");

  strcpy(s, "1");
  tstr_setlen(s, 1);
  ts_new = tstr_cpy(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "1st cpy to non-empty tstring, old len bigger than new len, len match");
  sput_fail_unless(strcmp(ts_new,s) == 0, "content match");

  strcpy(s, TSTR_CPYT_STRING3);
  tstr_setlen(s, strlen(TSTR_CPYT_STRING3));
  tstr_setro(ts1);
  ts_new = tstr_cpy(ts1, s);
  sput_fail_unless(ts_new != 0, "tstr_cpy() to readonly string does not cause an error");
  sput_fail_unless(ts_new != ts1, "tstr_cpy() to readonly string creates new tstring");
  tstr_setrw(ts1);
  ts_new = tstr_cpy(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "2nd cpy to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,s) == 0, "content match");
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) != 0, "tstring is freeable");

  for (int c=0;;) {
    s[c] = '1';
    ++c;
    s[c] = 0;
    if (c > (TSTR_CPYT_SIZE2-10)) break;
  }
  tstr_setlen(s, strlen(s));
  ts_new = tstr_cpy(ts2, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "1st 16 bit cpy to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,s) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  for (int c=0;;) {
    s[c] = '1';
    ++c;
    s[c] = 0;
    if (c > (TSTR_CPYT_SIZE2)) break;
  }
  tstr_setlen(s, strlen(s));
  if (ts2 != ts_new) {
    printf("INFO: new allocation did happen, new assigning freed ts2\n"); fflush(stdout);
    ts2 = ts_new;
  }
  ts_new = tstr_cpy(ts2, s);
  if (ts_new == 0) {
    sput_fail_unless(ts_new != 0, "2nd 16 bit cpy to non-empty tstring does not return 0 / no tstring");
  } else {
    sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "2nd 16 bit cpy to non-empty tstring, len match");
    sput_fail_unless(strcmp(ts_new,s) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
    sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif

  }
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

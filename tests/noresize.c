#define TSTR_TEST_SIZE1 62
#define TSTR_TEST_SIZE2 63
#define TSTR_TEST_SIZE3 65535
#define TSTR_TEST_SIZE4 65536
#define TSTR_TEST_SIZE5 524288

/**
 * Unit test for functions using/checking noresize flag
 */
static void test_noresize() {
  tstring ts1, ts2, ts_new;
  char buf[TSTR_TEST_SIZE5];

  /* initializing */
  ts1 = tstr_new(TSTR_TEST_SIZE1);

  /* tests */

  sput_fail_unless(ts1 != 0, "tstring created");
  sput_fail_unless(tstr_size(ts1) == TSTR_TEST_SIZE1, "size is "STR(TSTR_TEST_SIZE1));

  ts_new = tstr_resize(ts1, TSTR_TEST_SIZE2);
  sput_fail_unless(ts_new != 0, "resizing to "STR(TSTR_TEST_SIZE2));
  if (ts_new != 0) ts1 = ts_new;
  sput_fail_unless(tstr_size(ts1) == TSTR_TEST_SIZE2, "size is "STR(TSTR_TEST_SIZE2));
  sput_fail_unless(tstr_setnoresize(ts1) != 0, "tstr_setnoresize() failed on size being too low");

  ts_new = tstr_resize(ts1, TSTR_TEST_SIZE3);
  sput_fail_unless(ts_new != 0, "resizing to "STR(TSTR_TEST_SIZE3));
  if (ts_new != 0) ts1 = ts_new;
  sput_fail_unless(tstr_size(ts1) == TSTR_TEST_SIZE3, "size is "STR(TSTR_TEST_SIZE3));
  sput_fail_unless(tstr_setnoresize(ts1) != 0, "tstr_setnoresize() failed on size being too low");

  /* set noresize */

  ts_new = tstr_resize(ts1, TSTR_TEST_SIZE4);
  sput_fail_unless(ts_new != 0, "resizing to "STR(TSTR_TEST_SIZE4));
  if (ts_new != 0) ts1 = ts_new;
  sput_fail_unless(tstr_size(ts1) == TSTR_TEST_SIZE4, "size is "STR(TSTR_TEST_SIZE4));
  sput_fail_unless(tstr_setnoresize(ts1) == 0, "tstr_setnoresize() success on size being big enough");

  ts_new = tstr_resize(ts1, TSTR_TEST_SIZE5);
  sput_fail_unless(ts_new == 0, "resizing to "STR(TSTR_TEST_SIZE5)" failed because of noresize");

  memset(buf, 'A', TSTR_TEST_SIZE5);
  buf[TSTR_TEST_SIZE5-1] = 0;
  ts2 = tstr_createn(buf, TSTR_TEST_SIZE5);
  sput_fail_unless(ts2 != 0, "createn "STR(TSTR_TEST_SIZE5)" bytes for later copy/add");

  ts_new = tstr_cpycn(ts1, buf, TSTR_TEST_SIZE5);
  sput_fail_unless(ts_new == 0, "cpycn of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");
  ts_new = tstr_cpyc(ts1, buf);
  sput_fail_unless(ts_new == 0, "cpyc of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");
  ts_new = tstr_cpy(ts1, ts2);
  sput_fail_unless(ts_new == 0, "cpy of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");

  ts_new = tstr_catcn(ts1, buf, TSTR_TEST_SIZE5);
  sput_fail_unless(ts_new == 0, "catcn of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");
  ts_new = tstr_catc(ts1, buf);
  sput_fail_unless(ts_new == 0, "catc of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");
  ts_new = tstr_cat(ts1, ts2);
  sput_fail_unless(ts_new == 0, "cat of "STR(TSTR_TEST_SIZE5)" bytes failed because of noresize");

  /* set resize */

  sput_fail_unless(tstr_setresize(ts1) == 0, "tstr_setresize() success");

  ts_new = tstr_cpycn(ts1, buf, TSTR_TEST_SIZE5);
  sput_fail_unless(ts_new != 0, "cpycn of "STR(TSTR_TEST_SIZE5)" bytes success");
  if (ts_new != 0) ts1 = ts_new;
  ts_new = tstr_catcn(ts1, buf, TSTR_TEST_SIZE5);
  sput_fail_unless(ts_new != 0, "catcn of "STR(TSTR_TEST_SIZE5)" bytes success");
  if (ts_new != 0) ts1 = ts_new;
  //tstr_debugprinttstringn(ts1,"ts1");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

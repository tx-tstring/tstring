#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
// for tstr_debugprinttstring()
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

#define TSTR_SETLEN_SIZE1 63
#define TSTR_SETLEN_SIZE2 64
#define TSTR_SETLEN_SIZE3 65529
// for header32 with size 65526:
#define TSTR_SETLEN_SIZE4 65530

/**
 * Unit test for tstr_setlent() and partially tstr_len()
 */
static void test_tstr_setlen() {

  /* initializing */
  tstring ts1 = tstr_new(TSTR_SETLEN_SIZE1);
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  tstring ts2 = tstr_new(TSTR_SETLEN_SIZE2);
  tstring ts3 = tstr_new(TSTR_SETLEN_SIZE3);
#endif
  tstring ts4 = tstr_new(TSTR_SETLEN_SIZE4);
#endif

  /* tests */

  sput_fail_unless(tstr_setlen(0, 0) != 0, "tstring NULL");
  sput_fail_unless(tstr_setlen(ts1, UINT64_MAX - 2) != 0, "newlen too big");
  sput_fail_unless(tstr_setlen(ts1, 0) == 0, "newlen 0");
  sput_fail_unless(tstr_len(ts1) == 0, "tstr_len() not 0");
  sput_fail_unless(tstr_setlen(ts1, 63) == 0, "newlen 63");
  sput_fail_unless(tstr_len(ts1) == 63, "tstr_len() not 63");
  sput_fail_unless(tstr_len(0) == (size_t)(-1), "tstr_len() did not fail on null ptr");

  tstr_setro(ts1);
  sput_fail_unless(tstr_setlen(ts1, 0) == TSTR_ERROR_READONLY, "setlen error on readonly tstring");
  tstr_setrw(ts1);

#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(tstr_setlen(ts1, TSTR_SETLEN_SIZE2) != 0, "header6 newlen 64");
  sput_fail_unless(tstr_setlen(ts2, 0) == 0, "header16 newlen 0");
  sput_fail_unless(tstr_setlen(ts2, TSTR_SETLEN_SIZE2) == 0, "header16 newlen 64");
  sput_fail_unless(tstr_setlen(ts2, TSTR_SETLEN_SIZE2 + 1) != 0, "header16 newlen 65");
  sput_fail_unless(TSTR_TYPE(ts1) == TSTR_HEADERTYPE_6, "type for tstr_setlen(63) is header6");
  sput_fail_unless(TSTR_TYPE(ts2) == TSTR_HEADERTYPE_16, "type for tstr_setlen(64) is header16");
  sput_fail_unless(TSTR_TYPE(ts3) == TSTR_HEADERTYPE_16, "type for tstr_setlen(65529) is header16");
#endif
  sput_fail_unless(tstr_setlen(ts4, 0) == 0, "header32 newlen 0");
  sput_fail_unless(tstr_setlen(ts4, TSTR_SETLEN_SIZE4) == 0, "header32 newlen 65530");
  sput_fail_unless(tstr_setlen(ts4, TSTR_SETLEN_SIZE4 + 1) != 0, "header32 newlen 65531");
#endif
  //tstr_debugprinttstring(ts4);

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

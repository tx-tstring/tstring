/**
 * Unit test for tstr_cmp().
 */
static void test_tstr_cmp() {
  char buf1[30];
  char buf2[30];
  tstring ts1;
  tstring ts2;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);
  ts2 = tstr_buffer2tstring(buf2, sizeof(buf2), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "123\000567\000", 8);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(\"123\\0567\\0\") != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(\"123\\0567\\0\") returns same string");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_cpycn(\"123\\0567\\0\") returns tstring w len 8");

  ts_new = tstr_cpycn(ts2, "123\000567\000", 8);
  sput_fail_unless(tstr_cmp(ts1, ts2) == 0, "two equal tstrings");
  ts_new = tstr_cpycn(ts2, "123\000567\000", 7);
  sput_fail_unless(tstr_cmp(ts1, ts2) == 1, "first tstring greater length");
  ts_new = tstr_cpycn(ts2, "123\000567\0009", 9);
  sput_fail_unless(tstr_cmp(ts1, ts2) == -1, "second tstring greater length");
  ts_new = tstr_cpycn(ts2, "123\000567\001", 8);
  sput_fail_unless(tstr_cmp(ts1, ts2) != 0, "two different tstrings");
  ts_new = tstr_cpycn(ts1, "\000", 0);
  ts_new = tstr_cpycn(ts2, "\000", 0);
  sput_fail_unless(tstr_cmp(ts1, ts2) == 0, "two zero-length tstrings");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

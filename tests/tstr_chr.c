/**
 * Unit test for tstr_chr().
 */
static void test_tstr_chr() {
  char buf1[30];
  tstring ts1;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "123\0", 4);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(123\\0) != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(123\\0) returns same string");
  sput_fail_unless(tstr_len(ts_new) == 4, "tstr_cpycn(123\\0) returns tstring w len 4");

  ts_new = tstr_catcn(ts1, "456\0", 4);
  sput_fail_unless(ts_new != 0, "tstr_catcn(456\\0) != 0");
  sput_fail_unless(ts_new == ts1, "tstr_catcn(456\\0) returns same string");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_catcn(456\\0) returns tstring w len 8");

  sput_fail_unless(tstr_chr(ts1, '0') == 0, "tstr_chr('0') == 0");
  sput_fail_unless(tstr_chr(ts1, '1') == ts1, "tstr_chr('1') == tstring");
  sput_fail_unless(tstr_chr(ts1, '2') == ts1+1, "tstr_chr('2') == tstring+1");
  sput_fail_unless(tstr_chr(ts1, '3') == ts1+2, "tstr_chr('3') == tstring+2");
  sput_fail_unless(tstr_chr(ts1, 0) == ts1+3, "tstr_chr(0) == tstring+3");
  sput_fail_unless(tstr_chr(ts1, '4') == ts1+4, "tstr_chr('4') == tstring+4");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

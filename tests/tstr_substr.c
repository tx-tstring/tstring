/**
 * Unit test for tstr_substr().
 */
static void test_tstr_substr() {
  char buf1[30];
  tstring ts1;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "", 0);
  ts_new = tstr_substr(ts1, 0, 1);
  sput_fail_unless(ts_new == 0, "tstr_substr(ts, 0, 1) == 0 if ts has zero length");

  printf("Using tstring ts \"123\\0567\\0\"\n");fflush(stdout);
  ts_new = tstr_cpycn(ts1, "123\000567\000", 8);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(\"123\\0567\\0\") != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(\"123\\0567\\0\") returns same string");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_cpycn(\"123\\0567\\0\") returns tstring w len 8");

  ts_new = tstr_substr(ts1, 30, 1);
  sput_fail_unless(ts_new == 0, "tstr_substr(ts, 30, 1) == 0 if start is greater than ts length");

  ts_new = tstr_substr(ts1, 0, 1);
  sput_fail_unless(ts_new != 0, "tstr_substr(ts, 0, 1) != 0");
  sput_fail_unless(strcmp(ts_new,"12") == 0, "tstr_substr(ts, 0, 1) == \"12\"");

  ts_new = tstr_substr(ts1, 1, 0);
  sput_fail_unless(ts_new != 0, "tstr_substr(ts, 1, 0) != 0");
  sput_fail_unless(strcmp(ts_new,"2") == 0, "tstr_substr(ts, 1, 0) == \"2\"");

  ts_new = tstr_substr(ts1, 0, -1);
  sput_fail_unless(ts_new != 0, "tstr_substr(ts, 0, -1) != 0");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_len(ts_new) == 8");
  sput_fail_unless(memcmp(ts_new,"123\000567\000", 8) == 0, "memcmp(ts_new, ts, 8) == 0");

  ts_new = tstr_substr(ts1, 1, 6);
  sput_fail_unless(ts_new != 0, "tstr_substr(ts, 1, 6) != 0");
  sput_fail_unless(tstr_len(ts_new) == 6, "tstr_len(ts_new) == 6");
  sput_fail_unless(memcmp(ts_new,"23\000567", 6) == 0, "memcmp(ts_new, ts, 6) == 0");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define TSTR_CATC_STRING1 "1"
#define TSTR_CATC_STRING2 "123456789"
#define TSTR_CATC_STRING3 "123456789012345678901234567890"
#define TSTR_CATC_SIZE1 44
#define TSTR_CATC_SIZE2 65530

/**
 * Unit test for test_tstr_catc() and tstr_len().
 */
static void test_tstr_catc() {
  char buf[70000];
  tstring ts1;
  tstring ts2;
  tstring ts_new;

  /* initializing */
  char buf1[TSTR_CATC_SIZE1];
  ts1 = tstr_buffer2tstring(buf1, TSTR_CATC_SIZE1, 0);
  char *buf2 = malloc(TSTR_CATC_SIZE2);
  ts2 = tstr_buffer2tstring(buf2, TSTR_CATC_SIZE2, 1);

  /* tests */

  /* invalid arguments */
  sput_fail_unless(tstr_catc(0, TSTR_CATC_STRING1) == 0, "tstr_catc(0,...) returns 0");
  sput_fail_unless(tstr_catc(ts1, 0) == 0, "tstr_catc(...,0) returns 0");

  ts_new = tstr_catc(ts1, TSTR_CATC_STRING3);
  strcpy(buf, TSTR_CATC_STRING3);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "cat to empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "cat to empty tstring, content match");
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) == 0, "tstring is not freeable");

  ts_new = tstr_catc(ts_new, TSTR_CATC_STRING2);
  strcat(buf, TSTR_CATC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "1st cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) == 0, "tstring is not freeable");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  ts_new = tstr_catc(ts_new, TSTR_CATC_STRING2);
  strcat(buf, TSTR_CATC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "2nd cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 != ts_new, "tstring NOT same tstring as before");
#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
#endif
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) != 0, "tstring is freeable");

  ts_new = tstr_catc(ts_new, TSTR_CATC_STRING2);
  strcat(buf, TSTR_CATC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "3rd cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 != ts_new, "tstring NOT same tstring as before");
#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
#endif
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) != 0, "tstring is freeable");

  ts_new = tstr_catc(ts_new, TSTR_CATC_STRING2);
  strcat(buf, TSTR_CATC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "4th cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 != ts_new, "tstring NOT same tstring as before");
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) != 0, "tstring is freeable");


  for (int c=0;;) {
    buf[c] = '1';
    ++c;
    buf[c] = 0;
    if (c > (TSTR_CATC_SIZE2-10)) break;
  }
  ts_new = tstr_catc(ts2, buf);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "1st 16 bit cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  ts_new = tstr_catc(ts_new, TSTR_CATC_STRING2);
  strcat(buf, TSTR_CATC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "1st 16 bit cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

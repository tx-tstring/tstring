#define TSTR_CATT_STRING1 "1"
#define TSTR_CATT_STRING2 "123456789"
#define TSTR_CATT_STRING3 "123456789012345678901234567890"
#define TSTR_CATT_SIZE1 44
#define TSTR_CATT_SIZE2 65530

/**
 * Unit test for test_tstr_cat() and tstr_len().
 */
static void test_tstr_cat() {
  char sbuf[70000];
  tstring ts1;
  tstring ts2;
  tstring ts_new;
  tstring ts_new2;
  tstring s;

  /* initializing */
  char buf1[TSTR_CATT_SIZE1];
  ts1 = tstr_buffer2tstring(buf1, TSTR_CATT_SIZE1, 0);
  char *buf2 = malloc(TSTR_CATT_SIZE2);
  ts2 = tstr_buffer2tstring(buf2, TSTR_CATT_SIZE2, 1);
  s = tstr_buffer2tstring(sbuf, 70000, 0);

  /* tests */

  /* invalid arguments */
  sput_fail_unless(tstr_cat(0, s) == 0, "tstr_cat(0,...) returns 0");
  sput_fail_unless(tstr_cat(ts1, 0) == 0, "tstr_cat(...,0) returns 0");

  ts_new = tstr_cat(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "cat empty string to empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,s) == 0, "cat empty string to empty tstring, content match");
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");

  tstr_setro(ts_new);
  ts_new2 = tstr_catc(ts_new, "1");
  sput_fail_unless(ts_new2 != ts_new, "tstr_catc() returns new tstring on readonly tstring");
  tstr_setrw(ts_new);
  ts_new = tstr_catc(ts_new, "1");
  tstr_setro(ts1);
  ts_new = tstr_cat(ts1, s);
  sput_fail_unless(ts_new == ts1, "tstr_cat() empty string to readonly tstring returns same string");
  tstr_catc(s, "1");
  ts_new = tstr_cat(ts1, s);
  sput_fail_unless(ts_new != 0, "tstr_cat() to readonly tstring is possible with allocation");
  sput_fail_unless(ts_new != ts1, "tstr_cat() to readonly tstring returns new tstring");
  tstr_setrw(ts1);
  tstr_cpyc(s, "");
  ts_new = tstr_cat(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == 1, "cat empty tstring to small string, len match");
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");

  tstr_setlen(ts_new, 0);
  ts_new = tstr_cat(ts1, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "cat to empty tstring, len match");
  if (ts_new == 0) {
    sput_fail_unless(ts_new != 0, "tstring is 0 which must not happen");
  } else {
    sput_fail_unless(strcmp(ts_new,s) == 0, "cat to empty tstring, content match");
  }
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");

  for (int c=0;;) {
    s[c] = '1';
    ++c;
    s[c] = 0;
    tstr_setlen(s, c);
    if (c > (TSTR_CATT_SIZE2-10)) break;
  }
  ts_new = tstr_cat(ts2, s);
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "1st 16 bit cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new, s) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  tstring s2 = tstr_new(30); tstr_catc(s2, TSTR_CATT_STRING2);
  ts_new = tstr_cat(ts_new, s2);
  strcat(s, TSTR_CATT_STRING2);
  tstr_setlen(s, strlen(s));
  sput_fail_unless(tstr_len(ts_new) == tstr_len(s), "2nd 16 bit cat to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new, s) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

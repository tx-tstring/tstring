#define TSTR_CPYC_STRING1 "1"
#define TSTR_CPYC_STRING2 "123456789"
#define TSTR_CPYC_STRING3 "123456789012345678901234567890123456789012345678901234567890"
#define TSTR_CPYC_SIZE1 44
#define TSTR_CPYC_SIZE2 65530

/**
 * Unit test for tstr_cpyc() and tstr_len().
 */
static void test_tstr_cpyc() {
  char buf[70000];
  tstring ts1;
  tstring ts2;
  tstring ts_new;
  tstring ts_tmp;

  /* initializing */
  char buf1[TSTR_CPYC_SIZE1];
  ts1 = tstr_buffer2tstring(buf1, TSTR_CPYC_SIZE1, 0);
  char *buf2 = malloc(TSTR_CPYC_SIZE2);
  ts2 = tstr_buffer2tstring(buf2, TSTR_CPYC_SIZE2, 1);

  /* tests */

  /* invalid arguments */

  ts_tmp = tstr_cpyc(0, TSTR_CPYC_STRING1);
  sput_fail_unless(ts_tmp == 0, "tstr_cpyc(0,...) returns 0");
  ts_tmp = tstr_cpyc(ts1, 0);
  sput_fail_unless(ts_tmp == 0, "tstr_cpyc(...,0) returns 0");

  ts_new = tstr_cpyc(ts1, TSTR_CPYC_STRING2);
  strcpy(buf, TSTR_CPYC_STRING2);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "cpy to empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "cpy to empty tstring, content match");
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) == 0, "tstring is not freeable");

  tstr_setro(ts_new);
  ts_tmp = tstr_cpyc(ts_new, TSTR_CPYC_STRING1);
  sput_fail_unless(ts_tmp != 0, "tstr_cpyc() to readonly string works (with allocation)");
  sput_fail_unless(ts_tmp != ts_new, "tstr_cpyc() to readonly string returns new tstring");
  tstr_setrw(ts_new);
  ts_new = tstr_cpyc(ts_new, TSTR_CPYC_STRING1);
  strcpy(buf, TSTR_CPYC_STRING1);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "1st cpy to non-empty tstring, old len bigger than new len, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 == ts_new, "tstring same tstring as before");
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) == 0, "tstring is not freeable");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  ts_new = tstr_cpyc(ts_new, TSTR_CPYC_STRING3);
  strcpy(buf, TSTR_CPYC_STRING3);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "2nd cpy to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(ts1 != ts_new, "tstring NOT same tstring as before");
#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_6, "type is header6");
#endif
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif
  sput_fail_unless(TSTR_IS_FREEABLE(ts_new) != 0, "tstring is freeable");

  for (int c=0;;) {
    buf[c] = '1';
    ++c;
    buf[c] = 0;
    if (c > (TSTR_CPYC_SIZE2-10)) break;
  }
  ts_new = tstr_cpyc(ts2, buf);
  sput_fail_unless(ts_new != 0, "ts_new = tstr_cpyc(ts2, buf)");
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "1st 16 bit cpy to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_16, "type is header16");
#else
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
#endif

  for (int c=0;;) {
    buf[c] = '1';
    ++c;
    buf[c] = 0;
    if (c > (TSTR_CPYC_SIZE2)) break;
  }
  ts2 = ts_new;
  ts_new = tstr_cpyc(ts2, buf);
  sput_fail_unless(tstr_len(ts_new) == strlen(buf), "2nd 16 bit cpy to non-empty tstring, len match");
  sput_fail_unless(strcmp(ts_new,buf) == 0, "content match");
#ifndef _TSTR_SINGLE_STRUCT_USAGE
  sput_fail_unless(TSTR_TYPE(ts_new) == TSTR_HEADERTYPE_32, "type is header32");
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

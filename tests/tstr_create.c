#define TSTR_FROMC_SIZE1 63
#define TSTR_FROMC_SIZE2 64
#define TSTR_FROMC_SIZE3 65529
#define TSTR_FROMC_SIZE4 65530
#define TSTR_FROMC_SIZE5 524288

/**
 * Unit test for test_tstr_create(), tstr_size(), tstr_len() and direct access of chars in tstring.
 */
static void test_tstr_create() {
  tstring ts1;

  /* tests */

  sput_fail_unless(tstr_create(0) == 0, "0 returns 0");
  sput_fail_unless(tstr_create("") == 0, "empty string returns 0");

  ts1 = tstr_create("1");
  sput_fail_unless(ts1 != 0, "\"1\" created");
  sput_fail_unless(TSTR_IS_FREEABLE(ts1) != 0, "is freeable");
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  sput_fail_unless(TSTR_TYPE(ts1) == TSTR_HEADERTYPE_6, "type is header6");
#else
  sput_fail_unless(TSTR_TYPE(ts1) == TSTR_HEADERTYPE_32, "type is header32");
#endif

#ifndef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
  sput_fail_unless(tstr_size(ts1) == tstr_minimum_size, "size is tstr_minimum_size");
#endif
  sput_fail_unless(tstr_len(ts1) == 1, "len is 1");
  sput_fail_unless(strcmp(ts1,"1") == 0, "strcmp");
  sput_fail_unless(ts1[0] == '1', "tstring[0]");
  sput_fail_unless(ts1[1] == 0, "tstring[] terminating 0");
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

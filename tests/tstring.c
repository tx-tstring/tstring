#include <stdio.h>
#include <stdlib.h>
#include <sput.h>
#include <tstring.h>

#define STR1(s) #s
#define STR(s) STR1(s)

/** @file
 * \brief Unit tests of tstring library
 */

/*
 * EVIL! This is just a workaround for SPUT macros.
 * No single object files are built by Makefile.
 */
#include "tstr_buffer2tstring.c"
#include "tstr_new.c"
#include "tstr_catc.c"
#include "tstr_setlen.c"
#include "tstr_cat.c"
#include "tstr_cpyc.c"
#include "tstr_cpy.c"
#include "tstr_create.c"
#include "tstr_dup.c"
#include "tstr_free.c"
#include "tstr_chr.c"
#include "tstr_str.c"
#include "tstr_mem.c"
#include "tstr_tstr.c"
#include "tstr_substr.c"
#include "tstr_cmp.c"
#include "noresize.c"

int main(int argc, char *argv[])
{
    sput_start_testing();

    sput_enter_suite("tstring, tstr_init(), tstr_init_static(), tstr_buffer2tstring() & tstr_len()");
    sput_run_test(test_tstr_buffer2tstring);

    sput_enter_suite("tstring, tstr_new(), tstr_size() & tstr_free()");
    sput_run_test(test_tstr_new);

    sput_enter_suite("tstring, tstr_catc() & tstr_len()");
    sput_run_test(test_tstr_catc);

    sput_enter_suite("tstring, tstr_setlen()");
    sput_run_test(test_tstr_setlen);

    sput_enter_suite("tstring, tstr_cat() & tstr_len()");
    sput_run_test(test_tstr_cat);

    sput_enter_suite("tstring, tstr_cpyc() & tstr_len()");
    sput_run_test(test_tstr_cpyc);

    sput_enter_suite("tstring, tstr_cpy() & tstr_len()");
    sput_run_test(test_tstr_cpy);

    sput_enter_suite("tstring, tstr_dup()");
    sput_run_test(test_tstr_dup);

    sput_enter_suite("tstring, tstr_create() & tstr_size() & tstr_len()");
    sput_run_test(test_tstr_create);

    sput_enter_suite("tstring, tstr_free()");
    sput_run_test(test_tstr_free);

    sput_enter_suite("tstring, tstr_chr()");
    sput_run_test(test_tstr_chr);

    sput_enter_suite("tstring, tstr_str()");
    sput_run_test(test_tstr_str);

    sput_enter_suite("tstring, tstr_mem()");
    sput_run_test(test_tstr_mem);

    sput_enter_suite("tstring, tstr_tstr()");
    sput_run_test(test_tstr_tstr);

    sput_enter_suite("tstring, tstr_substr()");
    sput_run_test(test_tstr_substr);

    sput_enter_suite("tstring, tstr_cmp()");
    sput_run_test(test_tstr_cmp);

    sput_enter_suite("tstring, noresize flag");
    sput_run_test(test_noresize);

    sput_finish_testing();

    return sput_get_return_value();
}

/**
 * Unit test for tstr_mem().
 */
static void test_tstr_mem() {
  char buf1[30];
  tstring ts1;
  tstring ts_new;

  /* initializing */
  ts1 = tstr_buffer2tstring(buf1, sizeof(buf1), 0);

  /* tests */

  ts_new = tstr_cpycn(ts1, "123\000567\000", 8);
  sput_fail_unless(ts_new != 0, "tstr_cpycn(\"123\\0567\\0\") != 0");
  sput_fail_unless(ts_new == ts1, "tstr_cpycn(\"123\\0567\\0\") returns same string");
  sput_fail_unless(tstr_len(ts_new) == 8, "tstr_cpycn(\"123\\0567\\0\") returns tstring w len 8");

  sput_fail_unless(tstr_mem(ts1, "1", 1) == ts1, "tstr_mem(\"1\") == tstring");
  sput_fail_unless(tstr_mem(ts1, "123", 3) == ts1, "tstr_mem(\"123\") == tstring");
  sput_fail_unless(tstr_mem(ts1, "567", 3) == ts1+4, "tstr_mem(\"567\") == tstring+4");
  sput_fail_unless(tstr_mem(ts1, "23\000", 3) == ts1+1, "tstr_mem(\"23\\0\") == tstring+1");
  sput_fail_unless(tstr_mem(ts1, "24", 2) == 0, "tstr_mem(\"24\") == 0");
  sput_fail_unless(tstr_mem(ts1, "3\0005", 3) == ts1+2, "tstr_mem(\"3\\05\") == tstring+2");
  sput_fail_unless(tstr_mem(ts1, "\00056", 3) == ts1+3, "tstr_mem(\"\\056\") == tstring+3");
  sput_fail_unless(tstr_mem(ts1, "7\000", 2) == ts1+6, "tstr_mem(\"7\\0\") == tstring+6");

  sput_fail_unless(tstr_mem(ts1, "", 0) == 0, "tstr_mem(\"\") == 0");
  sput_fail_unless(tstr_mem(ts1, "1234567890", 10) == 0, "tstr_mem(\"1234567890\") == 0");
  ts_new = tstr_cpycn(ts1, "", 0);
  sput_fail_unless(tstr_len(ts_new) == 0, "tstr_cpycn(\"\") returns tstring w len 0");
  sput_fail_unless(tstr_mem(ts1, "1", 1) == 0, "tstr_mem(\"1\") on empty tstring returns 0");

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

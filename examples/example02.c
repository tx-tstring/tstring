#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

struct __attribute__ ((packed)) th {
  uint32_t len;
  uint32_t size;
  uint32_t flags;
  char buf[];
};

char *tcatcl(char *t, const char *s, size_t slen) {
  uint64_t cur_size, cur_len, new_len;
  void *th;

  th = (struct th *)(t - sizeof(struct th));
  cur_size = ((struct th *)th)->size;
  cur_len = ((struct th *)th)->len;
  new_len = cur_len + slen;
  memcpy(t+cur_len, s, slen);
  t[new_len] = 0;
  th = t - sizeof(struct th);
  ((struct th *)th)->len = new_len;
  
  return t;
}

int main(int argc, char * argv[]) {
  char sbuf[101];
  struct th *sth;
  char *t;

  sth = (struct th *) sbuf;
  t = sth->buf;
  strcpy(t,"aaa");
  sth->size = 100;
  sth->len = 3;
  t = tcatcl(t, "bbb\n", 4);
  write(1, t, sth->len);
  return 0;
}

//  uint32_t cur_flags = ((uint32_t *)t)[-1];
//  if (new_len > cur_size) {
//    void *th_new;
//    uint64_t new_alloc_len = new_len + 161;
//    th_new = realloc(th, new_alloc_len + sizeof(struct th));
//    if (th_new == 0) return 0;
//    ((struct th *)th_new)->size = new_alloc_len;
//    t = (char *)((char *)th_new + sizeof(struct th));
//
//  }

/*


x86-64 gcc 4.94 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl:
  pushq %rbp
  pushq %rbx
  movq %rdi, %rbx
  subq $8, %rsp
  movl -12(%rdi), %edi
  leaq (%rdx,%rdi), %rbp
  addq %rbx, %rdi
  call memcpy@PLT
  movb $0, (%rbx,%rbp)
  movl %ebp, -12(%rbx)
  addq $8, %rsp
  movq %rbx, %rax
  popq %rbx
  popq %rbp
  ret
main:
  subq $120, %rsp
  movl $7, %edx
  movl $1, %edi
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp)
  movl $100, 4(%rsp)
  movl $174219874, 15(%rsp)
  movb $0, 19(%rsp)
  movl $7, (%rsp)
  call write@PLT
  xorl %eax, %eax
  addq $120, %rsp
  ret


x86-64 gcc 5.4 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl:
  pushq %rbp
  pushq %rbx
  movq %rdi, %rbx
  subq $8, %rsp
  movl -12(%rdi), %edi
  leaq (%rdi,%rdx), %rbp
  addq %rbx, %rdi
  call memcpy@PLT
  movb $0, (%rbx,%rbp)
  movl %ebp, -12(%rbx)
  addq $8, %rsp
  movq %rbx, %rax
  popq %rbx
  popq %rbp
  ret
main:
  subq $120, %rsp
  movl $7, %edx
  movl $1, %edi
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp)
  movl $100, 4(%rsp)
  movl $174219874, 15(%rsp)
  movb $0, 19(%rsp)
  movl $7, (%rsp)
  call write@PLT
  xorl %eax, %eax
  addq $120, %rsp
  ret

x86-64 gcc 7.3 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl:
  pushq %rbp
  pushq %rbx
  movq %rdi, %rbx
  subq $8, %rsp
  movl -12(%rdi), %edi
  leaq (%rdi,%rdx), %rbp
  addq %rbx, %rdi
  call memcpy@PLT
  movb $0, (%rbx,%rbp)
  movl %ebp, -12(%rbx)
  addq $8, %rsp
  movq %rbx, %rax
  popq %rbx
  popq %rbp
  ret
main:
  subq $120, %rsp
  movl $7, %edx
  movl $1, %edi
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp)
  movl $100, 4(%rsp)
  movl $174219874, 15(%rsp)
  movb $0, 19(%rsp)
  movl $7, (%rsp)
  call write@PLT
  xorl %eax, %eax
  addq $120, %rsp
  ret

x86-64 gcc 8.2 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl:
  pushq %rbp
  pushq %rbx
  movq %rdi, %rbx
  subq $8, %rsp
  movl -12(%rdi), %edi
  leaq (%rdi,%rdx), %rbp
  addq %rbx, %rdi
  call memcpy@PLT
  movb $0, (%rbx,%rbp)
  movq %rbx, %rax
  movl %ebp, -12(%rbx)
  addq $8, %rsp
  popq %rbx
  popq %rbp
  ret
main:
  subq $120, %rsp
  movl $7, %edx
  movl $1, %edi
  movabsq $429496729607, %rax
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp)
  movb $0, 19(%rsp)
  movl $174219874, 15(%rsp)
  movq %rax, (%rsp)
  call write@PLT
  xorl %eax, %eax
  addq $120, %rsp
  ret


x86-64 clang 3.5.1 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl: # @tcatcl
  pushq %r14
  pushq %rbx
  pushq %rax
  movq %rdi, %r14
  movl -12(%r14), %ebx
  leaq (%r14,%rbx), %rdi
  addq %rdx, %rbx
  callq memcpy@PLT
  movb $0, (%r14,%rbx)
  movl %ebx, -12(%r14)
  movq %r14, %rax
  addq $8, %rsp
  popq %rbx
  popq %r14
  retq

main: # @main
  subq $104, %rsp
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp) # imm = 0x616161
  movl $100, 4(%rsp)
  movl $174219874, 15(%rsp) # imm = 0xA626262
  movb $0, 19(%rsp)
  movl $7, (%rsp)
  movl $1, %edi
  movl $7, %edx
  callq write@PLT
  xorl %eax, %eax
  addq $104, %rsp
  retq


x86-64 clang 7.0.0 -std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE  -O3  -fomit-frame-pointer

tcatcl: # @tcatcl
  pushq %r14
  pushq %rbx
  pushq %rax
  movq %rdi, %r14
  movl -12(%rdi), %ebx   # cur_len = ((struct th *)th)->len;
  leaq (%rdi,%rbx), %rdi # new_len = cur_len + slen;
  addq %rdx, %rbx
  callq memcpy@PLT       # memcpy()
  movb $0, (%r14,%rbx)   # t[new_len] = 0;
  movl %ebx, -12(%r14)   # ((struct th *)th)->len = new_len;
  movq %r14, %rax
  addq $8, %rsp
  popq %rbx
  popq %r14
  retq
main: # @main
  subq $104, %rsp
  leaq 12(%rsp), %rsi
  movl $6381921, 12(%rsp) # imm = 0x616161
  movl $174219874, 15(%rsp) # imm = 0xA626262
  movb $0, 19(%rsp)
  movabsq $429496729607, %rax # imm = 0x6400000007
  movq %rax, (%rsp)
  movl $1, %edi
  movl $7, %edx
  callq write@PLT
  xorl %eax, %eax
  addq $104, %rsp
  retq


*/

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

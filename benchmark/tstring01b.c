/**
 * tstring lib, benchmark/tstring01.c
 */

#include <stdio.h>
#include <tstring.h>
#include <rdtsc-measurement.h>

static tstring xtstr_catcn(tstring t, const char *s, size_t slen) {
  void *th;
  register uint64_t cur_len, new_len;

  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_len = ((struct tstring_header_32 *)th)->len;
  new_len = cur_len + slen;
  memcpy(t+cur_len, s, slen);
  //bcopy(s, t+cur_len, slen);
  t[new_len] = 0;
  ((struct tstring_header_32 *)th)->len = new_len;
  return t;
}

const size_t count = 1000;
const char concat_str[] = "A fairly long string for concatenation";
const size_t concat_size = sizeof(concat_str) - 1;
const size_t small_buf_size = 4788;
const size_t reg_buf_size = ((count + 1) * sizeof(concat_str)) + 32;
const size_t count2 = 4262145;
const size_t reg_buf_size2 = (count2 + 1) + 32;

/*
 * No benchmarks are done for concatenation without specifying the length as
 * this would be identicle to adding the execution time of strlen() to the
 * benchmarks bellow.
 */

static uint64_t tstring_cat_count_small() {
  tstring s;
  char sbuf[small_buf_size+1];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, small_buf_size, 0);
  for (size_t i = 0; i < count; i++) {
    s = tstr_catcn(s, concat_str, concat_size);
//    if (s == 0) {
//      printf("count=%lu\n", i);
//      exit(111);
//    }
  }
  no_optimize(s);
  b = rdtsc_stop();
  return b - a - RDTSC_OVERHEAD_MIN_CYCLES;
}

static uint64_t tstring_cat_count_reg() {
  tstring s;
  char sbuf[reg_buf_size+1];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, reg_buf_size, 0);
  for (size_t i = 0; i < count; i++) {
    s = xtstr_catcn(s, concat_str, concat_size);
  }
  no_optimize(s);
  b = rdtsc_stop();
  return b - a - RDTSC_OVERHEAD_MIN_CYCLES;
}

static uint64_t tstring_cat_count_reg2() {
  tstring s;
  char sbuf[reg_buf_size2+1];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, reg_buf_size2, 0);
  for (size_t i = 0; i < count2; i++) {
    //s = xtstr_catcn(s, "a", 1);
    s = tstr_catcn(s, "a", 1);
  }
  no_optimize(s);
  b = rdtsc_stop();
  //printf("len: %lu\n", tstr_len(s));
  return b - a - RDTSC_OVERHEAD_MIN_CYCLES;
}

static void batch_testing(unsigned long long (*testing_func)() ,size_t count_func, unsigned long long *sum, unsigned long long *cmin, unsigned long long *cmax) {
  size_t i = 1;
  unsigned long long cycles;
  *sum = *cmin = *cmax = cycles = testing_func();
  for (;;) {
    if (i >= count_func) break;
    cycles = testing_func();
    if (cycles < *cmin) {
      *cmin = cycles;
    }
    if (cycles > *cmax) {
      *cmax = cycles;
    }
    *sum += cycles;
    ++i;
  }
  return;
}

int main() {
  size_t count_func = 10000;
  unsigned long long sum, cmin, cmax;
  batch_testing(tstring_cat_count_small, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_small: min=%llu max=%llu avg=%f\n", cmin, cmax, ((double)(sum/(count_func/100)))/100);
  batch_testing(tstring_cat_count_reg, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_reg: min=%llu max=%llu avg=%f\n", cmin, cmax, ((double)(sum/(count_func/100)))/100);
  count_func /= 100;
  batch_testing(tstring_cat_count_reg2, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_reg2: min=%llu max=%llu avg=%f\n", cmin, cmax, ((double)(sum/(count_func/100)))/100);
  return 0;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

/**
 * tstring lib, benchmark/tstring01.c
 */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

#include <stdio.h>
#include <tstring.h>
#include <tstring-private.h>
#include <rdtsc-measurement.h>

static tstring xtstr_catcn(tstring t, const char *s, size_t slen) {
  uint64_t cur_len, new_len, cur_size;
  void *th;

  // get len
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_size = ((struct tstring_header_32 *)th)->size;
  cur_len = ((struct tstring_header_32 *)th)->len;
#else
  uint16_t cur_flags = ((uint16_t *)t)[-1];
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_size = (cur_flags & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
      cur_len = cur_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_size = ((struct tstring_header_16 *)th)->size;
      cur_len = ((struct tstring_header_16 *)th)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_size = ((struct tstring_header_32 *)th)->size;
      cur_len = ((struct tstring_header_32 *)th)->len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_size = ((struct tstring_header_64 *)th)->size;
      cur_len = ((struct tstring_header_64 *)th)->len;
      break;
#endif
    default:
      // something went wrong
      return 0;
      break;
  }
#endif

  new_len = cur_len + slen;
  memcpy(t+cur_len, s, slen);
  //bcopy(s, t+cur_len, slen);
  t[new_len] = 0;

  // set len
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  ((struct tstring_header_32 *)th)->len = new_len;
#else
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_flags = ((struct tstring_header_6 *)th)->flags & (TSTR_HEADERMOD_MASK | TSTR_HEADERTYPE_MASK | TSTR_HEADERTYPE6_SIZEMASK);
      ((struct tstring_header_6 *)th)->flags = cur_flags | new_len;
      break;
    case TSTR_HEADERTYPE_16:
      ((struct tstring_header_16 *)th)->len = new_len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      ((struct tstring_header_32 *)th)->len = new_len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      ((struct tstring_header_64 *)th)->len = new_len;
      break;
#endif
    default:
      return 0;
      break;
  }
#endif
  return t;
}

const size_t count = 400;
const char concat_str[] = "A fairly long string for concatenation";
const size_t concat_size = sizeof(concat_str) - 1;
const size_t small_buf_size = 4788;
const size_t count2 = 4262145;
#define REG_BUF_SIZE ((count * sizeof(concat_str)) + sizeof(concat_str) + sizeof(concat_str))
#define REG_BUF_SIZE2 (count2 + 33)

/*
 * No benchmarks are done for concatenation without specifying the length as
 * this would be identicle to adding the execution time of strlen() to the
 * benchmarks bellow.
 */

static uint64_t tstring_cat_count_small() {
  tstring s;
  char sbuf[small_buf_size+1];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, small_buf_size, 0);
  for (size_t i = 0; i < count; i++) {
    s = tstr_catcn(s, concat_str, concat_size);
//    if (s == 0) {
//      printf("count=%lu\n", i);
//      exit(111);
//    }
  }
  no_optimize(s);
  b = rdtsc_stop();
  b = b - a;
  if (b > RDTSC_OVERHEAD_MIN_CYCLES) b = b - RDTSC_OVERHEAD_MIN_CYCLES;
  return b;
}

static uint64_t tstring_cat_count_reg() {
  tstring s;
  char sbuf[REG_BUF_SIZE];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, REG_BUF_SIZE, 0);
  //printf("s=%p bufsize=%lu\n", (void *)s, REG_BUF_SIZE); fflush(stdout);
  for (size_t i = 0; i < count; i++) {
    s = xtstr_catcn(s, concat_str, concat_size);
//    printf("s=%p\n", (void *)s); fflush(stdout);
//    if (s == 0) {
//      printf("count=%lu\n", i);
//      exit(111);
//    }
  }
  no_optimize(s);
  b = rdtsc_stop();
  b = b - a;
  if (b > RDTSC_OVERHEAD_MIN_CYCLES) b = b - RDTSC_OVERHEAD_MIN_CYCLES;
  return b;
}

static uint64_t tstring_cat_count_reg2() {
  tstring s;
  char sbuf[REG_BUF_SIZE2+1];
  uint64_t a,b;

  a = rdtsc_start();
  s = tstr_buffer2tstring(sbuf, REG_BUF_SIZE2, 0);
  for (size_t i = 0; i < count2; i++) {
    //s = xtstr_catcn(s, "a", 1);
    s = tstr_catcn(s, "a", 1);
  }
  no_optimize(s);
  b = rdtsc_stop();
  b = b - a;
  if (b > RDTSC_OVERHEAD_MIN_CYCLES) b = b - RDTSC_OVERHEAD_MIN_CYCLES;
  return b;
}

static void batch_testing(uint64_t (*testing_func)() ,size_t count_func, uint64_t *sum, uint64_t *cmin, uint64_t *cmax) {
  size_t i = 1;
  uint64_t cycles;
  *sum = *cmin = *cmax = 0;
  cycles = testing_func();
  *sum = *cmin = *cmax = cycles;
  for (;;) {
    if (i >= count_func) break;
    cycles = testing_func();
    if (cycles < *cmin) {
      *cmin = cycles;
    }
    if (cycles > *cmax) {
      *cmax = cycles;
    }
    *sum += cycles;
    ++i;
  }
  return;
}

int main() {
  size_t count_func = 1000;
  uint64_t sum, cmin, cmax;
  //void *unused = alloc(1000); // workaround for realloc() bug in Fedora 18
  //alloc_free(unused);
  batch_testing(tstring_cat_count_small, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_small: min=%llu max=%llu\n", (unsigned long long)cmin, (unsigned long long)cmax);
  printf("tstring_cat_count_small: avg=%f\n", ((double)(sum/(count_func/100)))/100);fflush(stdout);
  batch_testing(tstring_cat_count_reg, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_reg: min=%llu max=%llu\n", (unsigned long long)cmin, (unsigned long long)cmax);
  printf("tstring_cat_count_reg: avg=%f\n", ((double)(sum/(count_func/100)))/100);fflush(stdout);
  count_func /= 50;
  batch_testing(tstring_cat_count_reg2, count_func, &sum, &cmin, &cmax);
  printf("tstring_cat_count_reg2: min=%llu max=%llu\n", (unsigned long long)cmin, (unsigned long long)cmax);
  printf("tstring_cat_count_reg2: avg=%f\n", ((double)(sum/(count_func/10)))/10);fflush(stdout);
  return 0;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

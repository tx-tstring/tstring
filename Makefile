# lib tstring Makefile

-include .config
name=tstring

# -DTSTR_ALLOW_16_BIT_ALIGNMENT will allow 16 bit alignment (default is 32) and will enable header6 and header16
# -DTSTR_ALLOW_EXABYTE_STRINGS will enable tstring with 64 bit size - 8 ExaBytes/EiB
#TSTRING_DEFINES=-DTSTR_ALLOW_16_BIT_ALIGNMENT -DTSTR_ALLOW_EXABYTE_STRINGS
#TSTRING_DEFINES=-DTSTR_ALLOW_EXABYTE_STRINGS
# you can override here lib/tstring-config.h
TSTRING_DEFINES=

WORKINGDIR=$(shell pwd)
DIRNAME=$(shell basename `pwd`)
VERSION=$(shell basename `pwd` |sed 's,$(name)-,,')
VERSION=$(shell date +%Y%m%d)
VERSION=$(shell grep ^Version: tstring.spec |cut -d" " -f2)
RPM_SPECS_DIR=$(shell rpm -E '%_specdir')
RPM_SOURCES_DIR=$(shell rpm -E '%_sourcedir')

ifeq ($(CONFIG_USE_DIET),y)
  DIET=diet
else
  DIET=
endif
ifeq ($(USE_EMBEDDEDBUILD),yes)
  EMBEDDED_CFLAGS=-DSMALL_BUILD
else
  EMBEDDED_CFLAGS=
endif
#HOSTCC=gcc
HOSTCC=cc
HOSTCC=$(shell if type gcc-4.1 >/dev/null 2>&1;then echo gcc-4.1;else echo cc;fi)
INCLUDES=-Ilib
NASM=/bin/false
ifeq ($(shell arch|egrep -q 'x86_64|i686' && echo y),y)
  #CPU_OPT=-march=nocona -mtune=nocona -msse2 --param=ssp-buffer-size=4
  CPU_OPT=
  NASM=nasm
else
  ifeq ($(shell arch|grep -q 'arm' && echo y),y)
    CPU_OPT=-march=armv6 -mfpu=vfp -mfloat-abi=hard
  else
    CPU_OPT=
  endif
endif
#BASE_CFLAGS=-std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE -pie $(TSTRING_DEFINES)
BASE_CFLAGS=-std=c99 -pedantic -D_FILE_OFFSET_BITS=64 -fPIE $(TSTRING_DEFINES)
#-O2 -fstack-protector -fomit-frame-pointer -falign-functions=1 -falign-jumps=1 -falign-loops=1 -Wall -march=pentium4
#-O2 -fomit-frame-pointer -Wall -march=nocona -mtune=nocona
#-O2 -march=nocona -mtune=nocona -msse2 -fomit-frame-pointer -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -pipe -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE=1 -D_GNU_SOURCE
#-O2 -march=nocona -mtune=nocona -msse2 -fomit-frame-pointer -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions --param=ssp-buffer-size=4 -pipe -I. -I.. -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE=1 -D_GNU_SOURCE
OPTFLAGS=-O3 $(CPU_OPT) -fomit-frame-pointer -D_FORTIFY_SOURCE=1 --param=ssp-buffer-size=4 -pipe
#OPTFLAGS=-Wl,-search_paths_first -Wl,-headerpad_max_install_names $(CPU_OPT)
GCC_WARNS_NEW=-Waddress -Warray-bounds=1 -Wbool-compare -Wchar-subscripts -Wcomment -Wenum-compare -Wformat -Wimplicit -Wimplicit-int -Wimplicit-function-declaration -Wlogical-not-parentheses -Wmain -Wmaybe-uninitialized -Wmemset-transposed-args -Wmissing-braces -Wnonnull -Wnonnull-compare -Wopenmp-simd -Wparentheses -Wpointer-sign -Wreturn-type -Wsequence-point -Wsizeof-pointer-memaccess -Wstrict-aliasing -Wstrict-overflow=1 -Wswitch -Wtautological-compare -Wtrigraphs -Wuninitialized -Wunknown-pragmas -Wunused-label -Wunused-value -Wunused-variable -Wvolatile-register-var
GCC_WARNS_COMPATIBLE=-Wchar-subscripts -Wcomment -Wformat -Wimplicit -Wimplicit-int -Wimplicit-function-declaration -Wmain -Wmissing-braces -Wnonnull -Wparentheses -Wpointer-sign -Wreturn-type -Wsequence-point -Wstrict-aliasing -Wswitch -Wtrigraphs -Wuninitialized -Wunknown-pragmas -Wunused-label -Wunused-value -Wunused-variable -Wvolatile-register-var
IS_OLD_GCC=$(shell { echo 'main(){}'|$(HOSTCC) $(GCC_WARNS_NEW) -E - >/dev/null 2>&1; echo $$?; })
IS_MAYBE_OSX=$(shell { echo 'main(){}'|$(HOSTCC) $(GCC_WARNS_NEW) -E - 2>&1|grep -q "did you mean '-Warray-bounds'"; echo $$?; })
ifeq ($(IS_MAYBE_OSX),0)
  IS_OLD_GCC=1
endif
ifeq ($(IS_OLD_GCC),1)
  GCC_WARNS=$(GCC_WARNS_COMPATIBLE)
else
  GCC_WARNS=$(GCC_WARNS_NEW)
endif
#EXTRACFLAGS=$(GCC_WARNS) -D_FORTIFY_SOURCE=1 --param=ssp-buffer-size=4 -Wl,-z,relro,-z,now -pipe
EXTRACFLAGS=-g
HOSTCFLAGS=$(BASE_CFLAGS) $(OPTFLAGS) $(GCC_WARNS) $(EXTRACFLAGS) $(EMBEDDED_CFLAGS) $(INCLUDES)
HOSTLD=ld
HOSTLDFLAGS=

CFLAGS=$(HOSTCFLAGS)
ALLCFLAGS=$(CFLAGS)
LDCFLAGS=$(ALLCFLAGS) -Wl,-z,noexecstack
CC=$(DIET) $(HOSTCC)
EXTRA_CFLAGS=
FUSEFLAGS=$(shell pkg-config fuse --cflags --libs || echo ===FUSEFLAGS_pkg-config_ERROR===)
LD=$(HOSTLD)
LDFLAGS=$(HOSTLDFLAGS)
ifneq ($(DIET),)
  EXTRA_CFLAGS+=-Wl,-lcompat
else
  EXTRA_CFLAGS+=-Wl,-lrt
endif
FUSE_FLAGS=$(shell pkg-config fuse --cflags --libs)
NASM_BIN_FLAGS=-f bin
NASM_ELF_FLAGS=-f elf
#SSTRIP=./sstrip-vladov
SSTRIP=tools/sstrip
HOSTSSTRIP=tools/sstrip-host
STRIP_ARGS=-s -R .note -R .comment
STRIP=$(HOSTSSTRIP)
USE_SSTRIP="sstrip"
ifeq ($(USE_SSTRIP),"sstrip")
  STRIP=$(HOSTSSTRIP)
else
  STRIP=strip $(STRIP_ARGS)
endif
RM=/bin/rm
CP=/bin/cp -PpR
MV=/bin/mv
LN=/bin/ln
ECHO=/bin/echo
INSTALL=install
AR=ar
ARFLAGS=rcs

DESTDIR=
#prefix=$(DESTDIR)/opt/$(name)
prefix=/usr/local
exec_prefix=$(prefix)
bindir=$(prefix)/bin
mandir=$(prefix)/share/man
sbindir=$(prefix)/sbin
sysconfdir=$(prefix)/etc
sysconfigdir=$(prefix)/etc/sysconfig
datadir=$(prefix)/share
includedir=$(prefix)/include
libdir=$(prefix)/lib
libexecdir=$(prefix)/libexec
localstatedir=/var
sharedstatedir=$(prefix)/com
infodir=$(prefix)/share/info

STATICLIB=lib$(name).a

LIBOBJS= \
	lib/alloc.o \
	lib/tstr_buffer2tstring.o \
	lib/tstr_cat.o \
	lib/tstr_catc.o \
	lib/tstr_catcn.o \
	lib/tstr_chr.o \
	lib/tstr_cmp.o \
	lib/tstr_cpy.o \
	lib/tstr_cpyc.o \
	lib/tstr_cpycn.o \
	lib/tstr_create.o \
	lib/tstr_createn.o \
	lib/tstr_dup.o \
	lib/tstr_free.o \
	lib/tstr_len.o \
	lib/tstr_mem.o \
	lib/tstr_new.o \
	lib/tstr_resize.o \
	lib/tstr_setlen.o \
	lib/tstr_setnoresize.o \
	lib/tstr_setresize.o \
	lib/tstr_setro.o \
	lib/tstr_setrw.o \
	lib/tstr_size.o \
	lib/tstr_str.o \
	lib/tstr_strerror.o \
	lib/tstr_substr.o \
	lib/tstr_tstr.o \
	lib/tstring-errno.o \

TESTSOBJS= \
	tests/tstring.o \

CLEANFILES= \

OBJS=$(SRCS:.c=.o)

.c.o:
	@echo CC -c -o $@ $<
	@$(CC) $(ALLCFLAGS) -c -o $@ $<

.c:
	$(CC) $(INCLUDES) $(CFLAGS) $< -o $@ $(STATICLIB)

.PHONY: clean usage man-clean man man-check

usage:
	@echo
	@echo '=== targets: all, check, install, clean'
	@echo
	@echo '=== Settings:'
	@echo
	@echo OPTFLAGS=$(OPTFLAGS) $(EXTRACFLAGS) $(EMBEDDED_CFLAGS) $(INCLUDES)
	@echo
	@echo HOSTCFLAGS=$(HOSTCFLAGS)
	@echo
	@echo CFLAGS=$(CFLAGS)
	@echo
	@echo 'waiting 5 seconds...'
	@echo
	@sleep 5
	$(MAKE) all

all: man man-check lib check

install-dirs:
	@-install -d -m 755 $(libdir) $(mandir)/man3 $(includedir)

install: install-dirs man-install lib check
	@echo
	@echo "=== using DESTDIR=$(DESTDIR) and prefix=$(prefix)"
	@echo
	@-install -d -m 755 $(DESTDIR)/$(libdir) $(DESTDIR)/$(mandir)/man3 $(DESTDIR)/$(includedir)
	install -p -m 644 $(STATICLIB) $(DESTDIR)/$(libdir)/
	tar c man/man3/*|( cd $(DESTDIR)/$(mandir)/..;tar x; )
	install -p -m 644 lib/tstring.h $(DESTDIR)/$(includedir)/

uninstall: man-clean man
	$(RM) -f $(libdir)/$(STATICLIB)
	cd man;for f in man3/*;do $(RM) -f $(mandir)/$$f;done
	$(RM) -f $(includedir)/tstring.h
	$(MAKE) man-clean

$(STATICLIB): tstring-config $(LIBOBJS)
	$(AR) $(ARFLAGS) $@ $(LIBOBJS)

lib: $(STATICLIB)

lib-clean:
	$(RM) -rf $(LIBOBJS) $(CLEANFILES) $(STATICLIB)

clean:: lib-clean check-clean benchmark-clean man-clean
	@echo $(RM) -f "\$$(LIBOBJS)" tags */*.o
	#$(MAKE) -C kconfig clean
	@$(RM) -f $(LIBOBJS) tags */tags */*.o

checkbuild: lib $(TESTSOBJS)
	$(CC) $(CFLAGS) -o tests/tstring tests/tstring.o $(STATICLIB)

check: checkbuild
	tests/tstring

check-clean:
	$(RM) -f tests/buffer $(TESTSOBJS) tests/*.o $(CLEANFILES) tests/tstring

tstring-config:
	[ -e lib/tstring-config-local.h ] || touch lib/tstring-config-local.h
	$(CC) $(ALLCFLAGS) -c -dM -E lib/tstr_is_freeable.c 2>/dev/null|grep -v __|egrep '_TSTR|TSTR_ALLOW'|sort

benchmarkbuild: lib benchmark-clean
	$(CC) $(CFLAGS) -o benchmark/tstring01 benchmark/tstring01.c $(STATICLIB)

benchmark: benchmarkbuild
	benchmark/tstring01

benchmark-clean:
	$(RM) -f benchmark/buffer $(TESTSOBJS) benchmark/*.o $(CLEANFILES) benchmark/tstring benchmark/tstring01

symlinks:
	-$(LN) -sf tstr_buffer2tstring.3 lib/tstr_init.3
	-$(LN) -sf tstr_buffer2tstring.3 lib/tstr_init_static.3
	-$(LN) -sf tstr_catc.3 lib/tstr_cat.3
	-$(LN) -sf tstr_catc.3 lib/tstr_catcn.3
	-$(LN) -sf tstr_cpyc.3 lib/tstr_cpycn.3
	-$(LN) -sf tstr_cpyc.3 lib/tstr_cpy.3
	-$(LN) -sf tstr_create.3 lib/tstr_createn.3

man: man-clean symlinks
	mkdir -p man/man3;cd lib && for f in *.3;do gzip -9 <$$f >../man/man3/$$f.gz;touch -r $$f ../man/man3/$$f.gz;done
	cd lib; ls -l *.3|awk '/ -> /{print$$9" "$$11}'|while read link target;do ( cd ../man/man3; rm -f "$$link".gz;$(LN) -s "$$target".gz "$$link".gz; ) ;done

man-check: man
	for f in tstr_chr tstr_str tstr_tstr tstr_mem tstr_setrw tstr_len tstr_free tstr_setro tstr_buffer2tstring tstr_new tstr_cpyc tstr_create tstr_dup tstr_catc tstring tstr_setlen tstr_size;do test -f man/man3/$$f.3.gz||exit 1;done
	for f in tstr_cpycn tstr_catcn tstr_createn tstr_init tstr_init_static;do test -L man/man3/$$f.3.gz||exit 1;done

man-install: man man-check
	tar c man/man3/*|( cd $(mandir)/..;tar x; )
	install -p -m 644 lib/tstring.h $(includedir)/

man-clean:
	$(RM) -rf man lib/tstr_cat.3 lib/tstr_catcn.3 lib/tstr_cpy.3 lib/tstr_cpycn.3 lib/tstr_createn.3 lib/tstr_init.3 lib/tstr_init_static.3

distclean: clean
	@$(RM) -rf .config generated .config.old .config.h include/config include/linux lib/tstring-config-local.h
	@-rmdir include
	#$(MAKE) savets

manhtml:
	rman -v # PolyglotMan
	for f in */*.[138];do echo $$f;rman -f html $$f|sed '/HREF="[^#]/s,A HREF,/A HREF,' >$$f.html;touch -r $$f $$f.html;done

tgz:
	@echo VERSION=$(VERSION)
	$(RM) -rf ../$(name)-$(VERSION);mkdir ../$(name)-$(VERSION)
	$(CP) benchmark doc docs doxyfile lib LICENSE.md Makefile README.md tests tstring.spec ../$(name)-$(VERSION)/
	$(RM) -f ../$(name)-$(VERSION)/lib/tstring-config-local.h
	cd ..;tar c $(name)-$(VERSION)|gzip -9 >$(name)-$(VERSION).tar.gz
	ls -ld ../$(name)-$(VERSION)/*
	ls -l ../$(name)-$(VERSION).tar.gz

rpm: tgz
	mkdir -p ~/rpmbuild/SOURCES/ ~/rpmbuild/SPECS/
	$(CP) ../$(name)-$(VERSION).tar.gz ~/rpmbuild/SOURCES/
	$(CP) tstring.spec ~/rpmbuild/SPECS/
	cd ~/rpmbuild/SPECS;rpmbuild -ba tstring.spec

manhtml-clean:
	$(RM) -f */*.[138].html

savets: distclean
	find . -type f -o -type d|egrep -v "\.git\/|\.git$$"|grep -v "\.timestamps"|sort|while read f;do echo $$(date +%s -r "$$f") "$$f";done >.timestamps

restorets:
	while read ts f;do touch -d@$$ts "$$f";done<.timestamps


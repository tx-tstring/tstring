// derived from https://github.com/google/highwayhash

#ifndef __GNUC__
#define __asm__ asm
#define __inline__ inline
#endif

// measurements were done on macOS High Sierra with clang and gcc
//
// some billion calls did not go below that, it
// should be safe to subtract this overhead from your measurements:
#define RDTSC_OVERHEAD_MIN_CYCLES 26
// if you do some hundred million measurements of your code than it
// should be valid to subtract from your average measurement this rdtsc average:
#define RDTSC_OVERHEAD_AVG_CYCLES 37
// more exactly it was 37.9

#undef RDTSC_ARCH_AARCH64
#undef RDTSC_ARCH_PPC
#undef RDTSC_ARCH_X64
#undef RDTSC_CLANG_VERSION
#undef RDTSC_GCC_VERSION
#if defined(__aarch64__) || defined(__arm64__)
#define RDTSC_ARCH_AARCH64 1
#endif
#if defined(__powerpc64__) || defined(_M_PPC)
#define RDTSC_ARCH_PPC 1
#endif
#if defined(__x86_64__) || defined(_M_X64)
#define RDTSC_ARCH_X64 1
#endif
#ifdef __clang__
#define RDTSC_CLANG_VERSION (__clang_major__ * 100 + __clang_minor__)
#endif
#ifdef __GNUC__
#define RDTSC_GCC_VERSION (__GNUC__ * 100 + __GNUC_MINOR__)
#endif

// Returns a 64-bit timestamp in unit of 'ticks'; to convert to seconds,
// divide by InvariantTicksPerSecond.
static __inline__ uint64_t rdtsc_start() {
  uint64_t t;
#if RDTSC_ARCH_PPC
  __asm__ volatile("mfspr %0, %1" : "=r"(t) : "i"(268));
#elif RDTSC_ARCH_AARCH64
  __asm__ volatile("mrs %0, cntvct_el0" : "=r"(t));
#elif RDTSC_ARCH_X64 && (RDTSC_CLANG_VERSION || RDTSC_GCC_VERSION)
  // this differs from highwayhash, see hadibrais.wordpress.com
  __asm__ volatile(
      "mfence\n\t"
      "lfence\n\t"
      "rdtsc\n\t"
      "shl $32, %%rdx\n\t"
      "or %%rdx, %0\n\t"
      "lfence"
      : "=a"(t)
      :
      // "memory" avoids reordering. rdx = TSC >> 32.
      // "cc" = flags modified by SHL.
      : "rdx", "memory", "cc");
#else
  //t = static_cast<uint64_t>(clock());
  //return just 0 to show that we can't use high resolution data
  t = 0;
#endif
  return t;
}

static __inline__ uint64_t rdtsc_stop() {
  uint64_t t;
#if RDTSC_ARCH_PPC
  __asm__ volatile("mfspr %0, %1" : "=r"(t) : "i"(268));
#elif RDTSC_ARCH_AARCH64
  __asm__ volatile("mrs %0, cntvct_el0" : "=r"(t));
#elif RDTSC_ARCH_X64 && (RDTSC_CLANG_VERSION || RDTSC_GCC_VERSION)
  // Use inline asm because __rdtscp generates code to store TSC_AUX (ecx).
  __asm__ volatile(
      "rdtscp\n\t"
      "shl $32, %%rdx\n\t"
      "or %%rdx, %0\n\t"
      "lfence"
      : "=a"(t)
      :
      // "memory" avoids reordering. rcx = TSC_AUX. rdx = TSC >> 32.
      // "cc" = flags modified by SHL.
      : "rcx", "rdx", "memory", "cc");
#else
  //t = static_cast<uint64_t>(clock());
  //return just 0 to show that we can't use high resolution data
  t = 0;
#endif
  return t;
}

// derived from Google benchmark
static __inline__ void no_optimize(void *p) {
#if defined(__clang__)
  // WHY? Why must r be preferred on clang?
  __asm__ volatile("" : "+r,m"(p) : : "memory");
#else
  __asm__ volatile("" : "+m,r"(p) : : "memory");
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Concatenate C string to tstring
 *
 * If s does not fit into free space of t then
 * a new tstring will be dynamically created to
 * hold t + s.
 * tstr_catcn() avoids calling strlen() as it is
 * required for tstr_catc().
 * Thus it can be much faster.
 * You should prefer tstr_catcn()
 * if you know the len of s.
 *
 * @param t The tstring to be extended.
 * @param s The C string to be appended.
 * @param slen The length of the C string.
 * @return The extended tstring.
 */
tstring tstr_catcn(tstring t, const char *s, size_t slen) {
  uint64_t cur_size, cur_len, new_len;
  void *th;
  int is_readonly = 0;
  uint16_t xflags = 0;

#ifdef _TSTR_DEBUG
  if (slen < 64) {
    printf("TSTR_DEBUG: tstr_catcn(%p, \"%s\", %lu)\n", (void *)t, s, slen); fflush(stdout);
  } else {
    printf("TSTR_DEBUG: tstr_catcn(%p, <long string>, %lu)\n", (void *)t, slen); fflush(stdout);
  }
#endif
  if (__unlikely(s == 0)) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_catcn() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(slen == 0)) { return t; }

  uint16_t cur_flags = ((uint16_t *)t)[-1];
  if (__unlikely(cur_flags & TSTR_READONLY)) { is_readonly = 1; }
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_size = ((struct tstring_header_32 *)th)->size;
  cur_len = ((struct tstring_header_32 *)th)->len;
  xflags = ((struct tstring_header_32 *)th)->xflags;
#else
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_size = (cur_flags & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
      cur_len = cur_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_size = ((struct tstring_header_16 *)th)->size;
      cur_len = ((struct tstring_header_16 *)th)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_size = ((struct tstring_header_32 *)th)->size;
      cur_len = ((struct tstring_header_32 *)th)->len;
      xflags = ((struct tstring_header_32 *)th)->xflags;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_size = ((struct tstring_header_64 *)th)->size;
      cur_len = ((struct tstring_header_64 *)th)->len;
      xflags = ((struct tstring_header_64 *)th)->xflags;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return 0;
      break;
  }
#endif

  // is current tstring big enough? remember the terminating 0
  // or do we need a new tstring because t is readonly?
  new_len = cur_len + slen;
  if (__unlikely((new_len >= cur_size) || __unlikely(is_readonly))) {

    if (__likely(!is_readonly)) {
      // if it is not ro then check for noresize
      if (xflags & TSTR_NORESIZE) {
#ifdef _TSTR_DEBUG
        printf("TSTR_DEBUG: tstr_catcn(): t IS noresize\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
        tstr_errno = TSTR_ERROR_NORESIZE;
#endif
        return 0;
      } else {
#ifdef _TSTR_DEBUG
        printf("TSTR_DEBUG: tstr_catcn(): t is NOT noresize\n"); fflush(stdout);
#endif
      }
    }

    tstring t_new;

#ifdef _TSTR_DEBUG
    if (is_readonly) {
      printf("TSTR_DEBUG: tstr_catcn(): new rw tstring required\n"); fflush(stdout);
    } else {
      printf("TSTR_DEBUG: tstr_catcn(): bigger tstring required\n"); fflush(stdout);
    }
#endif

    // need new or bigger tstring, calculate even more space
#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
    size_t new_size = tstr_calculate_bigger_size(new_len);
    if (__unlikely(new_size <= new_len)) new_size = new_len + 1; // dirty fallback
#else
    size_t new_size = new_len + tstr_minimum_size;
#endif

#ifdef _TSTR_SINGLE_STRUCT_USAGE
#ifdef _TSTR_USE_REALLOC

    if (__likely(cur_flags & TSTR_FREEABLE)) {
      void *th_new;
      uint64_t new_alloc_len = new_size + sizeof(struct tstring_header_32);
      const uint64_t alloc_limit = TSTR_REALLOC_ALLOC_LIMIT;

      if (__unlikely(new_alloc_len >= alloc_limit)) {
        goto no_realloc;
      }

      th_new = realloc(th, new_alloc_len);
#ifdef _TSTR_DEBUG
      printf("TSTR_DEBUG: tstr_catcn(): realloc(%p, %llu)=%p\n", th, new_alloc_len, th_new); fflush(stdout);
#endif
      if (__unlikely(th_new == 0)) goto no_realloc;
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
      if (th_new != th) {
        erase_from_memory(th_new, new_alloc_len, new_alloc_len);
      }
#endif

      ((struct tstring_header_32 *)th_new)->size = new_size;
      t = (tstring)((char *)th_new + sizeof(struct tstring_header_32));
      goto reallocated;
    }

no_realloc:

#endif
#endif

    t_new = tstr_new(new_size);
    if (__unlikely(t_new == 0)) {
      return 0;
    }

#ifdef _TSTR_DEBUG
    if (is_readonly) {
      printf("TSTR_DEBUG: tstr_catcn(): new rw tstring created, size=%lu\n", tstr_size(t_new)); fflush(stdout);
    } else {
      printf("TSTR_DEBUG: tstr_catcn(): bigger tstring created, size=%lu\n", tstr_size(t_new)); fflush(stdout);
    }
#endif

    // copy t data
    memcpy(t_new, t, cur_len);

    // free old tstring
    if (__likely(!is_readonly)) {
      tstr_free(t);
    }

    t = t_new;

  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
reallocated:
#endif

  // append new data
  memcpy(t+cur_len, s, slen);
  t[new_len] = 0;

  // now set len
  if (__unlikely(tstr_setlen(t, new_len) != TSTR_NOERROR)) {
    return 0;
  }
  
  return t;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

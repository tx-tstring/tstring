#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <stdio.h>

/** @file
 * Return a sub(t)string
 *
 * Create a new tstring containing a substring
 * from original tstring with the given indexes.
 * If length of t is 0 then 0 is returned.
 * If start index is greater than length of t
 * then 0 is returned.
 * If end index is greater than start index then
 * it is set to start index.
 * If end index is less than 0 then its value
 * is subtracted from the length to get the
 * actual end char index. That means that e.g.
 * a value of -1 takes the last char in the
 * tstring.
 *
 * @param t The tstring as source of the substring.
 * @param s The start char index of the substring.
 * @param e The end char index of the substring.
 * @return The new tstring with the substring or 0 on error.
 */
tstring tstr_substr(tstring t, size_t s, ssize_t e) {
  size_t t_len;
  tstring sub;

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_substr() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }

  t_len = tstr_len(t);
  if (__unlikely(t_len == 0)) { return 0; }
  if (__unlikely(s >= t_len)) { return 0; }
  //if ((e < 0) && (t_len < (0 - e))) { e = t_len - 1; }
  if (e < 0) { e = t_len + e; }
  if (e < s) { e = s; }

  t_len = e - s + 1;
  sub = tstr_new(t_len);
  if (__unlikely(sub == 0)) {
    return 0;
  }
  return tstr_cpycn(sub, t+s, t_len);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

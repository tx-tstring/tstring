#ifndef __ERASE_FROM_MEMORY_H__
#define __ERASE_FROM_MEMORY_H__ 1

#define __STDC_WANT_LIB_EXT1__ 1
#include <stdlib.h>
#include <string.h>
#ifdef _TSTR_DEBUG
#include <stdio.h>
#endif

static void *erase_from_memory(void *pointer, size_t size_data, size_t size_to_remove) {
  if (size_to_remove == 0) return 0;
#ifdef __STDC_LIB_EXT1__
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: memset_s(%p, %lu, %lu)\n", pointer, size_data, size_to_remove); fflush(stdout);
#endif
  memset_s(pointer, size_data, 0, size_to_remove);
#else
  if (size_to_remove > size_data) size_to_remove = size_data;
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: erase_from_memory() loop %p, size %lu\n", pointer, size_to_remove); fflush(stdout);
#endif
  volatile unsigned char *p = pointer;
  while (size_to_remove--) {
    *p++ = 0;
  }
#endif
  return pointer;
}

#endif
/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Get buffer allocation size of tstring
 *
 * @param t The tstring to get the buffer size of.
 * @return The data storage size of the tstring.
 */
size_t tstr_size(tstring t) {

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_size() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return (size_t) (-1);
  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  return ((struct tstring_header_32 *)(t - sizeof(struct tstring_header_32)))->size;
#else
  switch(TSTR_TYPE(t)) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6: return (((((struct tstring_header_6 *)(t - sizeof(struct tstring_header_6)))->flags) & TSTR_HEADERTYPE6_SIZEMASK) >>6); break;
    case TSTR_HEADERTYPE_16: return ((struct tstring_header_16 *)(t - sizeof(struct tstring_header_16)))->size; break;
#endif
    case TSTR_HEADERTYPE_32: return ((struct tstring_header_32 *)(t - sizeof(struct tstring_header_32)))->size; break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64: return ((struct tstring_header_64 *)(t - sizeof(struct tstring_header_64)))->size; break;
#endif
    default: break;
  }

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_size() error, tstring corrupted\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
  return (size_t) (-1);

#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

/** @file
 * \brief tstring-config.h - build time configuration file for tstring
 *
 * These are the default settings for build time.
 * They can be changed here or simply written to tstring-config-local.h.
 */


/**
 * _TSTR_ERRNO enables the use of tstr_errno (not thread-safe).
 */
#undef _TSTR_ERRNO


/**
 * _TSTR_DEBUG enables debugging output.
 */
#undef _TSTR_DEBUG


/**
 * _TSTR_USE_SECURE_ERASE_ON_FREE causes to fill memory with 0 before calling free() on it.
 */
#undef _TSTR_USE_SECURE_ERASE_ON_FREE


/**
 * _TSTR_USE_SECURE_ERASE_ON_INIT causes to fill memory with 0 before initializing a new tstring.
 */
#undef _TSTR_USE_SECURE_ERASE_ON_INIT


/**
 * _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS adds some extra free space when creating tstrings from C strings.
 * <br>
 * If it is enabled then it could give some speedup on many allocations.
 */
#define _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS


/**
 * TSTR_ALLOW_16_BIT_ALIGNMENT enables tstring_header_6 and tstring_header_16.
 * <br>
 * These require less space than tstring_header_32 but could have a performance drawback.
 * <br>
 * If it is enabled then _TSTR_USE_REALLOC is disabled.
 */
#undef TSTR_ALLOW_16_BIT_ALIGNMENT


/**
 * TSTR_ALLOW_EXABYTE_STRINGS enables tstring_header_64.
 * <br>
 * Enable TSTR_ALLOW_EXABYTE_STRINGS if you need strings >= 4 GB. It could cause build errors
 * on 32 bit platforms.
 * <br>
 * If it is enabled then _TSTR_USE_REALLOC is disabled.
 */
#undef TSTR_ALLOW_EXABYTE_STRINGS


/**
 * If _TSTR_DJB_ALLOC_BUFSIZE_KB is set to a value > 0 then
 * DJB's alloc() function will be used.
 * <br>
 * alloc() provides an additional layer before calling malloc()
 * by using a statically allocated buffer and delivering memory
 * from this buffer. If this buffer is fully used then it will
 * return memory by malloc(). With _TSTR_DJB_ALLOC_BUFSIZE_KB
 * you specify the size of the static buffer in KiB.
 * <br>
 * For tools terminating quickly and/or allocating only a low
 * amount of string space it can give you a performance benefit.
 * On long running tools or tools with many string allocs/frees
 * it does not provide a benefit.
 * <br>
 * Default: 0
 */
#define _TSTR_DJB_ALLOC_BUFSIZE_KB 0


/**
 * Define allocation limit used on realloc().
 * <br>
 * No reallocs with a value greater than this limit will be done.
 * realloc() is only used when no 16 bit or 64 bit headers are configured.
 * <br>
 * See TSTR_ALLOW_16_BIT_ALIGNMENT and TSTR_ALLOW_EXABYTE_STRINGS.
 * <br>
 * Default is 1024 MiB.
 */
#define TSTR_REALLOC_ALLOC_LIMIT 1024*1024*1024


/**
 * _TSTR_USE_EMBEDDED_CONFIGURATION is a meta configuration which uses a
 * reasonable configuration set targeted more towards embedded programming.
 * <br>
 * In particular it configures:
 * <ul>
 * <li>enable TSTR_ALLOW_16_BIT_ALIGNMENT</li>
 * <li>disable TSTR_ALLOW_EXABYTE_STRINGS</li>
 * <li>disable _TSTR_USE_SECURE_ERASE_ON_FREE</li>
 * <li>disable _TSTR_USE_SECURE_ERASE_ON_INIT</li>
 * <li>disable _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS</li>
 * <li>disable _TSTR_DEBUG</li>
 * <li>When a tstring must be increased in size then do just a small increase.</li>
 * </ul>
 */
#undef _TSTR_USE_EMBEDDED_CONFIGURATION


/**
 * You can make changes on the defines above or simply
 * write them to tstring-config-local.h.
 */
#include <tstring-config-local.h>

/**
 * Don't change anything below this line.
 */
#ifdef _TSTR_USE_EMBEDDED_CONFIGURATION
#ifndef TSTR_ALLOW_16_BIT_ALIGNMENT
#define TSTR_ALLOW_16_BIT_ALIGNMENT
#endif
#undef TSTR_ALLOW_EXABYTE_STRINGS
#undef _TSTR_USE_SECURE_ERASE_ON_FREE
#undef _TSTR_USE_SECURE_ERASE_ON_INIT
#undef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
#undef _TSTR_DEBUG
#endif
#ifdef _TSTR_DEBUG
#define _TSTR_ERRNO
#endif

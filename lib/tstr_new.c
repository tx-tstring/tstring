#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Create new tstring dynamically with given size.
 *
 * Create dynamically a new tstring.
 * If no memory could be requested then 0 is returned.
 * If given size is 0 then use tstr_minimum_size.
 *
 * @param size The data size of the tstring to be created.
 * @return The new tstring or 0 on error.
 */
tstring tstr_new(size_t size) {
  char *alloc_space;

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(%lu)\n", size); fflush(stdout);
#endif

  if (size < tstr_minimum_size) {
    size = tstr_minimum_size;
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(), use size=%lu\n", size); fflush(stdout);
#endif
  }

#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  if (size <= TSTR_HEADERTYPE6_LENMASK) {
    size += sizeof(struct tstring_header_6);
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(), use tstring size=%lu\n", size); fflush(stdout);
#endif
    goto allocate;
  }
  if (size <= (UINT16_MAX - sizeof(struct tstring_header_16))) {
    size += sizeof(struct tstring_header_16);
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(), use tstring size=%lu\n", size); fflush(stdout);
#endif
    goto allocate;
  }
#endif

  if (size <= (UINT32_MAX - sizeof(struct tstring_header_32))) {
    size += sizeof(struct tstring_header_32);
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(), use tstring size=%lu\n", size); fflush(stdout);
#endif
    goto allocate;
  }

#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  if (size <= (INT64_MAX - sizeof(struct tstring_header_64))) {
    size += sizeof(struct tstring_header_64);
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_new(), use tstring size=%lu\n", size); fflush(stdout);
#endif
    if (size <= INT64_MAX) {
      goto allocate;
    }
  }
#endif

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: size issue, tstr_new() returning 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
  tstr_errno = TSTR_ERROR_SIZETOOBIG;
#endif

  return 0;

allocate:

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: doing alloc(%lu)\n", size); fflush(stdout);
#endif
  alloc_space = alloc(size);
  if (alloc_space == 0) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: alloc() failed\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NOALLOC;
#endif
    return 0;
  }
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: erasing memory\n"); fflush(stdout);
#endif
  erase_from_memory(alloc_space, size, size);
#endif

  return tstr_buffer2tstring(alloc_space, size, 1);

}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

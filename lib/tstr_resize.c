#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Resize a tstring
 *
 * In most cases a new tstring will be created.
 * Just if the given size is already the size of
 * the tstring then nothing will be done and the
 * tstring will be returned.
 * <br>
 * If size 0 is given then the new string will have
 * size = len. That means that there is no space
 * left to grow for the string data
 * (like tstr_dup())..
 * <br>
 * If the given size is less then len of tstring
 * then a new tstring can't be created because
 * this would mean that the string data must be
 * truncated. In this case 0 will be returned as an
 * error value.
 *
 * @param t The tstring to be copied.
 * @return The resizelicated tstring.
 */
tstring tstr_resize(tstring t, size_t new_size) {
  size_t t_len;
  uint16_t xflags = 0;
  void *th;
  tstring s;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_resize(%p)\n", (void *)t); fflush(stdout);
#endif
  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_resize() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  t_len = tstr_len(t);
  if (t_len == new_size) {
    return t;
  }
  if (new_size == 0) { new_size = t_len; }
  if (__unlikely(new_size < t_len)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_resize() error, size too small\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_SIZETOOSMALL;
#endif
    return 0;
  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  xflags = ((struct tstring_header_32 *)th)->xflags;
#else
  switch(TSTR_TYPE(t)) {
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      xflags = ((struct tstring_header_32 *)th)->xflags;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      xflags = ((struct tstring_header_64 *)th)->xflags;
      break;
#endif
    default:
      break;
  }
#endif
  if (xflags & TSTR_NORESIZE) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cpycn(): t IS noresize\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NORESIZE;
#endif
    return 0;
  }

  s = tstr_new(new_size);
  if (__unlikely(s == 0)) return 0;
  return tstr_cpycn(s, t, t_len);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Find string in tstring
 *
 * @param t The tstring to be searched.
 * @param needle The string to be searched for.
 * @return pointer to the first needle in tstring or 0
 */
char *tstr_str(tstring t, const char *needle) {
  uint64_t cur_len;
  void *th;

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_str() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(*needle == 0)) { return t; }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_len = ((struct tstring_header_32 *)th)->len;
#else
  uint16_t cur_flags = ((uint16_t *)t)[-1];
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_len = cur_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_len = ((struct tstring_header_16 *)th)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_len = ((struct tstring_header_32 *)th)->len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_len = ((struct tstring_header_64 *)th)->len;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return 0;
      break;
  }
#endif

  return strstr(t, needle);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

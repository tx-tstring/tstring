#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Get length of data
 *
 * Return the internal len field of the tstring.
 * If t is 0 then 0 is returned.
 *
 * @param t The tstring to get the length of.
 * @return The length of the tstring.
 */
size_t tstr_len(tstring t) {

  if (__unlikely(t == 0)) {
    return (size_t) (-1);
  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  return ((struct tstring_header_32 *)(t - sizeof(struct tstring_header_32)))->len;
#else
  switch(TSTR_TYPE(t)) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6: return (((struct tstring_header_6 *)(t - sizeof(struct tstring_header_6)))->flags) & TSTR_HEADERTYPE6_LENMASK; break;
    case TSTR_HEADERTYPE_16: return ((struct tstring_header_16 *)(t - sizeof(struct tstring_header_16)))->len; break;
#endif
    case TSTR_HEADERTYPE_32: return ((struct tstring_header_32 *)(t - sizeof(struct tstring_header_32)))->len; break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64: return ((struct tstring_header_64 *)(t - sizeof(struct tstring_header_64)))->len; break;
#endif
    default: break;
  }
  return (size_t) (-1);

#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

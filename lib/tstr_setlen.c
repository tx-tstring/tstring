#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Set new len of tstring.
 *
 * The new length of the tstring must not be greater
 * than the size of the tstring.
 * This function could be useful if the data of
 * a tstring was modified by a C string library
 * function and requires its len field to be
 * manually updated.
 *
 * @param t The tstring to be updated.
 * @param new_len The new length of the tstring.
 * @return 0 on success
 */
int tstr_setlen(tstring t, size_t new_len) {
  uint64_t cur_size, cur_len;
  void *th;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_setlen(%p, %lu)\n", (void *)t, new_len); fflush(stdout);
#endif

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setlen() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return TSTR_ERROR_NULLARGUMENT;
  }

  uint16_t cur_flags = ((uint16_t *)t)[-1];
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_size = ((struct tstring_header_32 *)th)->size;
  cur_len = ((struct tstring_header_32 *)th)->len;
#else
  // get the size
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      th = (t - sizeof(struct tstring_header_6));
      cur_size = (cur_flags & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
      cur_len = cur_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_size = ((struct tstring_header_16 *)th)->size;
      cur_len = ((struct tstring_header_16 *)th)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_size = ((struct tstring_header_32 *)th)->size;
      cur_len = ((struct tstring_header_32 *)th)->len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_size = ((struct tstring_header_64 *)th)->size;
      cur_len = ((struct tstring_header_64 *)th)->len;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_DEBUG
      printf("TSTR_DEBUG: tstr_setlen() error, tstring internally corrupted\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return TSTR_ERROR_BROKENTSTRING;
      break;
  }
#endif


  if (__unlikely(cur_len == new_len)) { return TSTR_NOERROR; }
  if (__unlikely(cur_flags & TSTR_READONLY)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setlen() error, t is read-only\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_READONLY;
#endif
    return TSTR_ERROR_READONLY;
  }

  // is len valid / not greater than size?
  if (new_len > cur_size) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setlen() error, len > size\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_ILLEGALLEN;
#endif
    return TSTR_ERROR_ILLEGALLEN;
  }


  // now set len
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  ((struct tstring_header_32 *)th)->len = new_len;
#else
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_flags = ((struct tstring_header_6 *)th)->flags & (TSTR_HEADERMOD_MASK | TSTR_HEADERTYPE_MASK | TSTR_HEADERTYPE6_SIZEMASK);
      ((struct tstring_header_6 *)th)->flags = cur_flags | new_len;
      break;
    case TSTR_HEADERTYPE_16:
      ((struct tstring_header_16 *)th)->len = new_len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      ((struct tstring_header_32 *)th)->len = new_len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      ((struct tstring_header_64 *)th)->len = new_len;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_DEBUG
      printf("TSTR_DEBUG: tstr_setlen() error, tstring internally corrupted\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return TSTR_ERROR_BROKENTSTRING;
      break;
  }
#endif
  
  t[new_len] = 0;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_setlen() done\n"); fflush(stdout);
#endif
  return TSTR_NOERROR;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

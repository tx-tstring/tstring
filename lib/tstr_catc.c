#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Concatenate C string to tstring
 *
 * If s does not fit into free space of t then
 * a new tstring will be dynamically created to
 * hold t + s.
 * You should prefer tstr_catcn()
 * if you know the len of s.
 *
 * @param t The tstring to be extended.
 * @param s The C string to be appended.
 * @return The extended tstring.
 */
tstring tstr_catc(tstring t, const char *s) {

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_catc() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(s == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_catc() error, s is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  // in case of t being readonly simply allocate a new string
  //if (__unlikely(TSTR_IS_READONLY(t))) { return 0; }
  return tstr_catcn(t, s, strlen(s));
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

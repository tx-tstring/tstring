#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Create tstring from C string
 *
 * If s or slen is 0 then no tstring will be created.
 * tstr_createn() avoids calling strlen() as it is
 * required for tstr_create().
 * Thus it can be much faster.
 * You should prefer tstr_createn()
 * if you know the len of s.
 *
 * @param s The C string from which the tstring will be created.
 * @param slen The length of the C string.
 * @return The new tstring. 0 on error.
 */
tstring tstr_createn(const char *s, size_t slen) {
  tstring t;

  if (__unlikely(s == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_createn() error, s is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(slen == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_createn() error, slen is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_SIZETOOSMALL;
#endif
    return 0;
  }

#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
  t = tstr_new(tstr_calculate_bigger_size(slen));
#else
  t = tstr_new(slen + tstr_minimum_size);
#endif
  if (__unlikely(t == 0)) { return 0; }

  return tstr_cpycn(t, s, slen);
  
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

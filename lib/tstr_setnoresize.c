#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Make string non-resizeable, set noresize flag.
 *
 * When a tstring is marked "noresize"
 * then it can't be resized.
 * This is a protection against memory fillup
 * by data from unknown sources.
 * <br>
 * Only tstrings with size > 65535 can be set
 * to noresize.
 *
 * @param t The tstring to be updated.
 * @return 0 on success
 */
int tstr_setnoresize(tstring t) {
  void *th;
  uint16_t *xflags;
  size_t size=0;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_setnoresize(%p)\n", (void *)t); fflush(stdout);
#endif

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setnoresize() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return TSTR_ERROR_NULLARGUMENT;
  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  xflags = &((struct tstring_header_32 *)th)->xflags;
  size = ((struct tstring_header_32 *)th)->size;
#else
  switch(TSTR_TYPE(t)) {
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      xflags = &((struct tstring_header_32 *)th)->xflags;
      size = ((struct tstring_header_32 *)th)->size;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      xflags = &((struct tstring_header_64 *)th)->xflags;
      size = ((struct tstring_header_64 *)th)->size;
      break;
#endif
    default:
#ifdef _TSTR_DEBUG
      printf("TSTR_DEBUG: tstr_setresize() error, size < 65536, size too small\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_SIZETOOSMALL;
#endif
      return TSTR_ERROR_SIZETOOSMALL;
      break;
  }
#endif

  if (size <= UINT16_MAX) {
    // xflags only exist in tstring_header_32 and tstring_header_64
    // resize only supported for size > 65535
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setresize() error, size < 65536, size too small\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_SIZETOOSMALL;
#endif
    return TSTR_ERROR_SIZETOOSMALL;
  }

  *xflags |= TSTR_NORESIZE;

  return TSTR_NOERROR;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

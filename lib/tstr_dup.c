#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Duplicate a tstring
 *
 * Create a new tstring with same content like
 * original string.
 * The size of the new tstring is at least
 *   tstr_minimum_size
 * or on greater length exactly the length
 * (which means that adding even just one char
 * a re-allocation will happen).
 * <br>
 * Note: Flags like e.g. 'read-only' are not
 * copied.
 *
 * @param t The tstring to be copied.
 * @return The duplicated tstring.
 */
tstring tstr_dup(tstring t) {
  size_t t_len;
  tstring s;

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_dup() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }

  t_len = tstr_len(t);
  s = tstr_new(t_len);
  if (__unlikely(s == 0)) { return 0; }
  return tstr_cpycn(s, t, t_len);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

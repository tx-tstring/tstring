#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Set tstring read-only.
 *
 * When a tstring is marked readonly
 * then its data is protected against changes
 * by tstring functions.
 *
 * @param t The tstring to be updated.
 * @return 0 on success
 */
int tstr_setro(tstring t) {
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_setro(%p)\n", (void *)t); fflush(stdout);
#endif

  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_setro() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return TSTR_ERROR_NULLARGUMENT;
  }

  uint16_t cur_flags = ((uint16_t *)t)[-1];
  ((uint16_t *)t)[-1] = cur_flags | TSTR_READONLY;

  return TSTR_NOERROR;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

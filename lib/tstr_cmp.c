#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Compare two tstrings.
 *
 * @param t The first tstring to be compared.
 * @param s The second tstring to be compared.
 * @return return zero if the two tstrings are identical
 */
int tstr_cmp(tstring t, tstring s) {
  uint64_t cur_len, s_len;
  void *th;

  if ((__unlikely(t == 0)) && (__unlikely(s == 0))) {
    return 0;
  }
  if (__unlikely(t == 0)) {
    return -1;
  } else {
    if (__unlikely(s == 0)) {
      return 1;
    }
  }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_len = ((struct tstring_header_32 *)th)->len;
#else
  uint16_t cur_flags = ((uint16_t *)t)[-1];
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_len = cur_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_len = ((struct tstring_header_16 *)th)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_len = ((struct tstring_header_32 *)th)->len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_len = ((struct tstring_header_64 *)th)->len;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return TSTR_ERROR_BROKENTSTRING;
      break;
  }
#endif

  s_len = tstr_len(s);
  if ((__unlikely(s_len == 0)) && (__unlikely(cur_len == 0))) return 0;
  if (s_len < cur_len) return 1;
  if (s_len > cur_len) return -1;

  return memcmp(t, s, (size_t) cur_len);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

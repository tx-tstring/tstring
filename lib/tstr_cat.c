#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Concatenate tstring to tstring
 *
 * If s does not fit into free space of t then
 * a new tstring will be dynamically created to
 * hold t + s.
 *
 * @param t The tstring to be extended.
 * @param s The tstring to be appended.
 * @return The extended tstring.
 */
tstring tstr_cat(tstring t, const tstring s) {
  uint64_t cur_s_len;
  void *sh;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_cat(%p, %p)\n", (void *)t, (void *)s); fflush(stdout);
#endif
  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cat() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(s == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cat() error, s is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  // in case of t being readonly simply allocate a new string
  //if (__unlikely(TSTR_IS_READONLY(t))) { return 0; }

#ifdef _TSTR_SINGLE_STRUCT_USAGE
  sh = (struct tstring_header_32 *)(s - sizeof(struct tstring_header_32));
  cur_s_len = ((struct tstring_header_32 *)sh)->len;
#else
  uint16_t cur_s_flags = ((uint16_t *)s)[-1];
  uint16_t cur_s_type = TSTR_HEADERTYPE_MASK & cur_s_flags;
  switch(cur_s_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_s_len = cur_s_flags & TSTR_HEADERTYPE6_LENMASK;
      break;
    case TSTR_HEADERTYPE_16:
      sh = (s - sizeof(struct tstring_header_16));
      cur_s_len = ((struct tstring_header_16 *)sh)->len;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      sh = (s - sizeof(struct tstring_header_32));
      cur_s_len = ((struct tstring_header_32 *)sh)->len;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      sh = (s - sizeof(struct tstring_header_64));
      cur_s_len = ((struct tstring_header_64 *)sh)->len;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return 0;
      break;
  }
#endif

  return tstr_catcn(t, s, cur_s_len);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Copy C string to tstring
 *
 * If s does not fit into t then
 * a new tstring will be dynamically created to
 * hold s.
 * You should prefer tstr_cpycn()
 * if you know the len of s.
 *
 * @param t The tstring to be overwritten.
 * @param s The C string to be copied.
 * @return The overwritten tstring.
 */
tstring tstr_cpyc(tstring t, const char *s) {

#ifdef _TSTR_DEBUG
  size_t slen;
  if (s!=0) {
    slen = strlen(s);
    if (slen < 64) {
      printf("TSTR_DEBUG: tstr_cpyc(%p, \"%s\"), size=%lu\n", (void *)t, s, slen); fflush(stdout);
    } else {
      printf("TSTR_DEBUG: tstr_cpyc(%p, <long string>), size=%lu\n", (void *)t, slen); fflush(stdout);
    }
  } else {
    printf("TSTR_DEBUG: tstr_cpyc(%p, 0)\n", (void *)t); fflush(stdout);
  }
#endif
  if (__unlikely(s == 0)) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(t == 0)) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }

  // in case of t being readonly simply allocate a new string
  //if (__unlikely(TSTR_IS_READONLY(t))) { return 0; }
  return tstr_cpycn(t, s, strlen(s));
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

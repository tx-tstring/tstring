#include <tstring.h>

/** This is an usual non-threadsafe errno */
int tstr_errno = TSTR_NOERROR;

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#ifndef __LIBTSTRING_H
#define __LIBTSTRING_H

/** @file
 * \brief tstring - a lightweight C string buffer library
 *
 * <p>
 * tstring, http://www.tuxad.com
 * </p>
 *
 * <p>
 * <strong>Introduction</strong>
 * </p>
 *
 * <p>
 * The goal of this string library is to provide a more safe and
 * more simple replacement of the original evil C string "library".
 * </p>
 * <p>
 * Well, to keep the original "C spirit" it would have been better to
 * use a very different struct with avoiding of counters like len and
 * size and provide pointers instead of those counters:<br>
 * Pointer like e.g. "end_of_string" and "end_of_buffer".<br>
 * A "pointer approach" would also provide a more easy "alignment"
 * with a better compatibility between different arch sizes like 16 bit,
 * 32 bit and 64 bit.
 * </p>
 * <p>
 * But decision was made to go with this counter solution. Most (all?)
 * other auxiliary/additional C string libraries use counters. The SDS
 * library also uses "dynamical struct headers". But it's missing
 * features which are provided by other solutions like Lockless Dynamic
 * Strings. This tstring library is my approach and future tests will
 * show which features will survive and which will be kicked out... :-)
 * </p>
 *
 * <p>
 * <strong>Current features</strong>
 * </p>
 *
 * <ul>
 * <li>tstring is compatible to 'char *' of classic C string functions - no conversion required</li>
 * <li>size and length are handled to avoid buffer overflows</li>
 * <li>automated growth of strings</li>
 * <li>lightweight</li>
 * <li>able to secure erase memory on free() and new()</li>
 * <li>able to store binary data including '\0'</li>
 * <li>supports static and dynamic allocation</li>
 * <li>supports read-only strings</li>
 * <li>data is stored CPU cache friendly within struct of tstring (no pointer)</li>
 * <li>ANSI C, compatible with many environments like e.g. POSIX, Linux, *BSD</li>
 * <li>widely configurable with self explaining tstring-config.h</li>
 * <li>configurable for different struct header sizes and alignments</li>
 * <li>suitable for exabyte strings or even embedded usage</li>
 * <li>unit tested</li>
 * <li>comes with man pages ("man tstring" as starting point)</li>
 * </ul>
 *
 * <p>
 * <strong>Requirements</strong>
 * </p>
 *
 * <ul>
 * <li>POSIX/Unix system / ANSI C</li>
 * <li>gcc >= 4 (on macOS also clang tested)</li>
 * <li>make</li>
 * </ul>
 *
 * <p>
 * <strong>Build</strong>
 * </p>
 *
 * <ul>
 * <li>optionally: Edit lib/tstring-config.h (or simply add your own lib/tstring-config-local.h)</li>
 * <li>make</li>
 * </ul>
 *
 * <p>
 * <strong>Currently implemented functions</strong>
 * </p>
 *
 * <p>
 * A tstring is directly compatible to 'char *' C strings.
 * But besides tstr_* functions which provide string handling with
 * overflow protection there are also some functions and ways to "convert"
 * a C string to a tstring:
 * </p>
 *
 * <ul>
 * <li>conversion/creation: tstr_new(), tstr_create(), tstr_createn(), tstr_init(), tstr_init_static(), tstr_buffer2tstring()</li>
 * <li>substitute of strlen(): tstr_len()</li>
 * <li>substitutes of strcat(): tstr_cat(), tstr_catc(), tstr_catcn()</li>
 * <li>substitutes of strcpy(): tstr_cpy(), tstr_cpyc(), tstr_cpycn()</li>
 * <li>substitute of strdup(): tstr_dup()</li>
 * <li>substitute of free(): tstr_free()</li>
 * <li>tstring custom funcs: tstr_setlen(), tstr_setro(), tstr_setrw(), tstr_size()</li>
 * <li>strchr() substitute: tstr_chr()</li>
 * <li>strstr() substitute: tstr_str(), tstr_tstr()</li>
 * <li>memmem() substitute: tstr_mem(), tstr_tstr()</li>
 * </ul>
 */

#if defined(__linux__) || defined(__linux) || defined(__LINUX__)
#define _GNU_SOURCE
#endif
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <tstring-errno.h>

/** base definition: tstring is a pointer to the actual buffer - the struct headers differ in size and are placed BEFORE the buffer */
typedef char* tstring;

/** minimum size of a tstring */
static const uint16_t tstr_minimum_size = 31;

/** actually not an error: in some cases 0 means that no error did occur */
#define TSTR_NOERROR 0
/** generic/unspecified error value if none of the specified custom error value does match */
#define TSTR_ERROR_UNSPECIFIED -1
/** error value if a required function argument was NULL */
#define TSTR_ERROR_NULLARGUMENT -2
/** error value if a write attempt was made to a read only tstring */
#define TSTR_ERROR_READONLY -3
/** error value if (re-) allocating did fail */
#define TSTR_ERROR_NOALLOC -4
/** error value if size is too big to be handled by tstring */
#define TSTR_ERROR_SIZETOOBIG -5
/** error value if len is illegal */
#define TSTR_ERROR_ILLEGALLEN -6
/** error value if an attribute of a tstring header has an invalid value meaning that the tstring is broken */
#define TSTR_ERROR_BROKENTSTRING -7
/** error value if a resize was tried on a tstring with flag noresize */
#define TSTR_ERROR_NORESIZE -8
/** error value if a dup/cpy was tried on a tstring with flag nodup */
#define TSTR_ERROR_NODUP -9
/** error value if size is too small to be used by tstring */
#define TSTR_ERROR_SIZETOOSMALL -10
/** error value if static tstring was tried to free'd */
#define TSTR_ERROR_FREEONSTATIC -11

/** modifier mask */
#define TSTR_HEADERMOD_MASK (3UL << 14)
/** highest flag bit specifies if it was statically or dynamically allocated */
#define TSTR_FREEABLE (1UL << 15)
/** flag bit 14 specifies if data should be readonly */
#define TSTR_READONLY (1UL << 14)
/** xflags highest flag bit specifies 'noresize' */
#define TSTR_NORESIZE (1UL << 15)

/** header type mask */
#define TSTR_HEADERTYPE_MASK (3UL << 12)
/** header type 6 */
#define TSTR_HEADERTYPE_6 (0UL << 12)
/** header type 16 */
#define TSTR_HEADERTYPE_16 (1UL << 12)
/** header type 32 */
#define TSTR_HEADERTYPE_32 (2UL << 12)
/** header type 64 */
#define TSTR_HEADERTYPE_64 (3UL << 12)
/** header type 6 size mask */
#define TSTR_HEADERTYPE6_SIZEMASK (63UL << 6)
/** header type 6 len mask */
#define TSTR_HEADERTYPE6_LENMASK (63UL)

// some macros
#define TSTR_HEADERFLAGS(p) (((uint16_t *)p)[-1])
#define TSTR_IS_FREEABLE(p) ((((uint16_t *)p)[-1]) & TSTR_FREEABLE)
#define TSTR_IS_READONLY(p) ((((uint16_t *)p)[-1]) & TSTR_READONLY)
#define TSTR_TYPE(p) (TSTR_HEADERTYPE_MASK & ((uint16_t *)p)[-1])
#define tstr_init(a,b) tstr_buffer2tstring(a,b,1)
#define tstr_init_static(a,b) tstr_buffer2tstring(a,b,0)

// tstring struct header

/** This is the header for tstrings with up to 64 bytes */
struct
#ifndef DOXYGEN
__attribute__ ((packed))
#endif
tstring_header_6 {
  /**
   * misc flags like e.g. read only, freeable, header type and also size and len are embedded into flags for 6 bit
   * <br>
   * <ul>
   * <li>bit 15: freeable</li>
   * <li>bit 14: read-only</li>
   * <li>bit 13/12: header type</li>
   * <li>bit 6-11: size</li>
   * <li>bit 0-5: len</li>
   * </ul>
   */
  uint16_t flags;
  /** buf is used for local data storage */
  char buf[];
};

/** This is the header for tstrings with up to 64 KiB */
struct
#ifndef DOXYGEN
__attribute__ ((packed))
#endif
tstring_header_16 {
  /** the actual length of the stored data */
  uint16_t len;
  /** the allocated size of the buffer */
  uint16_t size;
  /** misc flags like e.g. read only, freeable, header type */
  uint16_t flags;
  /** buf is used for local data storage */
  char buf[];
};

/**
 * This is the header for tstrings with up to 4 gigs - it's the standard struct available on all compile time configurations.
 *
 * <p>
 * This is the only header which exists in all tstring compile
 * time configurations: It exists without 16 bit header and
 * without 8 Exa bytes tstrings.<br>
 * It is actually the "base tstring struct".<br>
 * <br>
 * Its struct members are:<br>
 * </p>
 * <ul>
 * <li>len: The actual length of the stored string data.</li>
 * <li>size: The allocated size of the tstring's buffer.</li>
 * <li>flags: Misc. flags like "freeable", "read-only" and more</li>
 * <li>buf: The actual buffer of the tstring</li>
 * </ul>
 */
struct
#ifndef DOXYGEN
__attribute__ ((packed))
#endif
tstring_header_32 {
  /** the actual length of the stored data */
  uint32_t len;
  /** the allocated size of the buffer */
  uint32_t size;
  /** for future use */
  uint16_t xflags;
  /** misc flags like e.g. read only, freeable, header type */
  uint16_t flags;
  /** buf is used for local data storage */
  char buf[];
};

/** This is the header for tstrings with up to 8 ExaBytes (exactly 8 EiB) */
struct
#ifndef DOXYGEN
__attribute__ ((packed))
#endif
tstring_header_64 {
  /** the actual length of the stored data */
  uint64_t len;
  /** the allocated size of the buffer */
  uint64_t size;
  /** for future use */
  uint16_t xflags;
  /** misc flags like e.g. read only, freeable, header type */
  uint16_t flags;
  /** buf is used for local data storage */
  char buf[];
};

/** helper macro for static sizing of buffers for tstring, e.g. char buf[TSTR_SIZE(20)] */
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
#define TSTR_SIZE(x) ( (x) < 65 ? (x)+2 : ( (x) < 65536 ? (x)+6 : (x)+12 ) )
#else
#define TSTR_SIZE(x) ( (x) < 65536 ? (x)+6 : (x)+12 ) 
#endif

size_t tstr_len(tstring p);
size_t tstr_size(tstring p);
tstring tstr_resize(tstring t, size_t new_size);
tstring tstr_buffer2tstring(char *buf, size_t size, uint16_t freeable);
tstring tstr_new(size_t size);
tstring tstr_catc(tstring t, const char *s);
tstring tstr_catcn(tstring t, const char *s, size_t slen);
tstring tstr_cat(tstring t, const tstring s);
int tstr_setlen(tstring t, size_t new_len);
tstring tstr_cpyc(tstring t, const char *s);
tstring tstr_cpycn(tstring t, const char *s, size_t slen);
tstring tstr_cpy(tstring t, const tstring s);
tstring tstr_dup(tstring t);
int tstr_free(tstring t);
tstring tstr_createn(const char *s, size_t slen);
tstring tstr_create(const char *s);
int tstr_setro(tstring t);
int tstr_setrw(tstring t);
char *tstr_chr(tstring t, int c);
char *tstr_str(tstring t, const char *needle);
char *tstr_mem(tstring t, const char *little, size_t little_len);
char *tstr_tstr(tstring t, tstring s);
tstring tstr_substr(tstring t, size_t s, ssize_t e);
int tstr_cmp(tstring t, tstring s);
int tstr_setnoresize(tstring t);
int tstr_setresize(tstring t);
const char *tstr_strerror(int errnum);

#endif
/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

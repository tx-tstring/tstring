#ifndef __LIBTSTRINGPRIVATE_H
#define __LIBTSTRINGPRIVATE_H

/** @file
 * \brief A lightweight C string buffer library - internal definitions
 *
 * <p>
 * tstring, tstring-private.h, FWB, http://www.tuxad.com
 * </p>
 *
 * <p>
 * <strong>Abstract</strong>
 * </p>
 *
 * <p>
 * This header file is only used internally by tstring lib calls and for
 * compile time configurations.<br>
 * </p>
 *
 * <p>
 * <strong>Compile time configuration</strong>
 * </p>
 *
 * <p>
 * The default and always existing tstring header/struct
 * is tstring_header_32 with 32 bit addressing (up to 4 GiB).
 * The header is 32 bit aligned. It is also possible to use 16
 * bit alignment and get two additional headers for smaller
 * tstrings and lower memory usage. On 64 bit system it is also
 * possible to use 64 bit tstrings.
 * </p>
 *
 * <p>
 * Defines:
 * </p>
 *
 * <ul>
 * <li>TSTR_ALLOW_16_BIT_ALIGNMENT: use 16 bit header alignment and enable tstring_header_6 and tstring_header_16</li>
 * <li>TSTR_ALLOW_EXABYTE_STRINGS: enable 64 bit tstrings</li>
 * <li>_TSTR_USE_SECURE_ERASE_ON_FREE: enable secure erasing of tstrings before freeing</li>
 * <li>_TSTR_USE_SECURE_ERASE_ON_INIT: enable secure erasing of tstrings after initializing (seldom required)</li>
 * <li>_TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS: When creating new tstrings e.g. from C strings then add extra free space</li>
 * <li>_TSTR_USE_EMBEDDED_CONFIGURATION (like a "meta define"): disable _TSTR_USE_SECURE_ERASE*, disable _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS, enable TSTR_ALLOW_16_BIT_ALIGNMENT, disable TSTR_ALLOW_EXABYTE_STRINGS</li>
 * </ul>
 *
 * <p>
 * The configuration can be changed in lib/tstring-config.h (or simply add your own lib/tstring-config-local.h).
 * </p>
 *
 */

#if defined(__linux__) || defined(__linux) || defined(__LINUX__)
#define _GNU_SOURCE
#endif

/** The configuration can be changed in lib/tstring-config.h (or simply add your own lib/tstring-config-local.h). */
#include <tstring-config.h>

#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
#include <erase_from_memory.h>
#endif
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
#include <erase_from_memory.h>
#endif

#ifdef _GNU_SOURCE
#include <features.h>
#endif

#include <tstring.h>

#ifdef _TSTR_DEBUG
#include <stdio.h>
#endif

/** _TSTR_USE_REALLOC: use realloc() instead of new malloc(); only used when no 16 bit or 64 bit tstrings are configured*/
#define _TSTR_USE_REALLOC
#ifndef TSTR_REALLOC_ALLOC_LIMIT
#define TSTR_REALLOC_ALLOC_LIMIT 1024*1024*1024
#endif

#if __GNUC__ > 4
_Pragma("GCC diagnostic push")
_Pragma("GCC diagnostic ignored \"-Wpedantic\"")
#ifndef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#warning %%%%%%%%%%%%%%% You must not include <tstring-private.h>! Please include just <tstring.h>. %%%%%%%%%%%%%%% 
#endif
_Pragma("GCC diagnostic pop")
#endif

#if __GNUC__ > 5
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#endif

int tstr_is_freeable(tstring t);

/** define an EOF if not defined */
#ifndef EOF
#define EOF (-1)
#endif

/* gcc >= 3 required */
#ifndef __expect
#define __expect(foo,bar) __builtin_expect((long)(foo),bar)
#endif
#ifndef __likely
/* macros borrowed from Linux kernel */
#define __likely(foo) __expect((foo),1)
#define __unlikely(foo) __expect((foo),0)
#endif

#undef _TSTR_SINGLE_STRUCT_USAGE
#ifndef TSTR_ALLOW_16_BIT_ALIGNMENT
#  define _TSTR_USE_XFLAGS
#  ifndef TSTR_ALLOW_EXABYTE_STRINGS
#    define _TSTR_SINGLE_STRUCT_USAGE
#  endif
#else
#  undef _TSTR_USE_XFLAGS
#endif

/** PD by Dan Bernstein: alloc(). Can be configured with _TSTR_DJB_ALLOC_BUFSIZE_KB */
void *alloc(size_t n);
/** PD by Dan Bernstein: alloc_free() */
void alloc_free(char *x);

#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
/**
 * Calculate size for bigger tstring
 *
 * @param s_old The old size.
 * @return The new size.
 */
static uint64_t tstr_calculate_bigger_size(uint64_t s_old) {
#ifdef _TSTR_USE_EMBEDDED_CONFIGURATION
  if (s_old < 20) {
    return 31;
  }
  if (s_old < 36) {
    return 47;
  }
  if (s_old < 52) {
    return 64;
  }
  // relative: 125%
  return s_old + (s_old >> 2);
#else
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  if (s_old < 48) {
    return 63;
  }
  if (s_old < 100) {
    return 255;
  }
#endif
  if (s_old < 256) {
    return 1024;
  }
  if (s_old < 2048) {
    return 8192;
  }
  if (s_old < 16384) {
    return 65536;
  }
  // relative: 400%
  if (s_old < 262144) {
    return s_old * 4;
  }
  // relative: 200% for >256KiB
  return s_old * 2;
#endif
}
#endif

#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
/**
 * Init a given buffer as tstring_header_6.
 *
 * This should not be called directly but only via e.g. tstr_buffer2tstring().
 * No error checks (e.g. on arguments) are done.
 * If "freeable" is != 0 then the tstring is marked as "freeable" (is
 * dynamically allocated).
 *
 * @param buf The buffer to be initialized.
 * @param size The size of buf.
 * @param freeable Boolean specifying dynamic allocation.
 * @return The initialized tstring.
 */
static tstring tstr_init6(char *buf, size_t size, uint16_t freeable) {
  struct tstring_header_6 *t6;
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_init6(%p, %llu, %hu)\n", (void *)buf, (unsigned long long)size, freeable); fflush(stdout);
#endif
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  erase_from_memory(buf, size, size);
#endif
  if (freeable != 0) freeable = TSTR_FREEABLE;
  t6 = (struct tstring_header_6*) buf;
  t6->flags = TSTR_HEADERTYPE_6 | freeable | (((size - sizeof(struct tstring_header_6)) << 6));
  t6->buf[0] = 0;
  return t6->buf;
}

/**
 * Init a given buffer as tstring_header_16.
 *
 * This should not be called directly but only via e.g. tstr_buffer2tstring().
 * No error checks (e.g. on arguments) are done.
 * If "freeable" is != 0 then the tstring is marked as "freeable" (is
 * dynamically allocated).
 *
 * @param buf The buffer to be initialized.
 * @param size The size of buf.
 * @param freeable Boolean specifying dynamic allocation.
 * @return The initialized tstring.
 */
static tstring tstr_init16(char *buf, size_t size, uint16_t freeable) {
  struct tstring_header_16 *t16;

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_init16(%p, %llu, %hu)\n", (void *)buf, (unsigned long long)size, freeable); fflush(stdout);
#endif
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  erase_from_memory(buf, size, size);
#endif
  if (freeable != 0) freeable = TSTR_FREEABLE;
  t16 = (struct tstring_header_16*) buf;
  t16->size = size - sizeof(struct tstring_header_16);
  t16->len = 0;
  t16->flags = TSTR_HEADERTYPE_16 | freeable;
  t16->buf[0] = 0;
  return t16->buf;
}
#endif

/**
 * Init a given buffer as tstring_header_32.
 *
 * This should not be called directly but only via e.g. tstr_buffer2tstring().
 * No error checks (e.g. on arguments) are done.
 * If "freeable" is != 0 then the tstring is marked as "freeable" (is
 * dynamically allocated).
 *
 * @param buf The buffer to be initialized.
 * @param size The size of buf.
 * @param freeable Boolean specifying dynamic allocation.
 * @return The initialized tstring.
 */
static tstring tstr_init32(char *buf, uint64_t size, uint16_t freeable) {
  struct tstring_header_32 *t32;

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_init32(%p, %llu, %hu)\n", (void *)buf, (unsigned long long) size, freeable); fflush(stdout);
#endif
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  erase_from_memory(buf, size, size);
#endif
  if (freeable != 0) freeable = TSTR_FREEABLE;
  t32 = (struct tstring_header_32*) buf;
  t32->size = size - sizeof(struct tstring_header_32);
  t32->len = 0;
  t32->flags = TSTR_HEADERTYPE_32 | freeable;
#ifdef _TSTR_USE_XFLAGS
  t32->xflags = 0;
#endif
  t32->buf[0] = 0;
  return t32->buf;
}

#ifdef TSTR_ALLOW_EXABYTE_STRINGS
/**
 * Init a given buffer as tstring_header_64.
 *
 * This should not be called directly but only via e.g. tstr_buffer2tstring().
 * No error checks (e.g. on arguments) are done.
 * If "freeable" is != 0 then the tstring is marked as "freeable" (is
 * dynamically allocated).
 *
 * @param buf The buffer to be initialized.
 * @param size The size of buf.
 * @param freeable Boolean specifying dynamic allocation.
 * @return The initialized tstring.
 */
static tstring tstr_init64(char *buf, uint64_t size, uint16_t freeable) {
  struct tstring_header_64 *t64;

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_init64(%p, %llu, %hu)\n", (void *)buf, (unsigned long long)size, freeable); fflush(stdout);
#endif
#ifdef _TSTR_USE_SECURE_ERASE_ON_INIT
  erase_from_memory(buf, size, size);
#endif
  if (freeable != 0) freeable = TSTR_FREEABLE;
  t64 = (struct tstring_header_64*) buf;
  t64->size = (uint64_t)(size - sizeof(struct tstring_header_64));
  t64->len = 0;
  t64->flags = TSTR_HEADERTYPE_64 | freeable;
#ifdef _TSTR_USE_XFLAGS
  t32->xflags = 0;
#endif
  t64->buf[0] = 0;
  return t64->buf;
}
#endif


#include <tstring-errno.h>
#ifdef _TSTR_DEBUG
static void inline tstr_debugprinttstring(tstring t) {
  uint64_t size, len;
  void *th;
  char printbuf[65];
  int rem;

  fflush(stdout);
  printf("TSTR_DEBUG: tstring %p\n", (void *)t);
#ifdef _TSTR_ERRNO
  printf("TSTR_DEBUG: errno=%d %s\n", tstr_errno, tstr_strerror(tstr_errno));
#endif
  if (t == 0) {
    return;
  }
  rem = snprintf(printbuf, 64, "%s", t);
  printf("TSTR_DEBUG:   content=%s", printbuf);
  if (rem >= 64) {
    printf("...");
  }
  printf("\n");
  uint16_t xflags;
  uint16_t flags = ((uint16_t *)t)[-1];
#ifndef _TSTR_SINGLE_STRUCT_USAGE
  uint16_t type = TSTR_HEADERTYPE_MASK & flags;
#endif
  printf("TSTR_DEBUG:   flags=%x\n", flags);
  printf("TSTR_DEBUG:   is freeable=%d\n", (flags & TSTR_FREEABLE) != 0);
  printf("TSTR_DEBUG:   is readonly=%d\n", (flags & TSTR_READONLY) != 0);
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  printf("TSTR_DEBUG: is tstring_header_32\n");
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  size = ((struct tstring_header_32 *)th)->size;
  len = ((struct tstring_header_32 *)th)->len;
  xflags = ((struct tstring_header_32 *)th)->xflags;
  printf("TSTR_DEBUG:   th=%p\n", th);
  printf("TSTR_DEBUG:   xflags=%x\n", xflags);
  printf("TSTR_DEBUG:   size=%llu\n", (unsigned long long)size);
  printf("TSTR_DEBUG:   len=%llu\n", (unsigned long long)len);
  printf("TSTR_DEBUG:   buf=%p\n", ((void *)((struct tstring_header_32 *)th)->buf));
#else
  switch(type) {
    case TSTR_HEADERTYPE_6:
      th = (t - sizeof(struct tstring_header_6));
      size = (flags & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
      len = flags & TSTR_HEADERTYPE6_LENMASK;
      printf("TSTR_DEBUG:   is tstring_header_6\n");
      printf("TSTR_DEBUG:   th=%p\n", th);
      printf("TSTR_DEBUG:   size=%llu\n", (unsigned long long)size);
      printf("TSTR_DEBUG:   len=%llu\n", (unsigned long long)len);
      printf("TSTR_DEBUG:   header=%p\n", th);
      printf("TSTR_DEBUG:   buf=%p\n", ((void *)((struct tstring_header_6 *)th)->buf));
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      size = ((struct tstring_header_16 *)th)->size;
      len = ((struct tstring_header_16 *)th)->len;
      printf("TSTR_DEBUG:   is tstring_header_16\n");
      printf("TSTR_DEBUG:   th=%p\n", th);
      printf("TSTR_DEBUG:   size=%llu\n", (unsigned long long)size);
      printf("TSTR_DEBUG:   len=%llu\n", (unsigned long long)len);
      printf("TSTR_DEBUG:   header=%p\n", th);
      printf("TSTR_DEBUG:   buf=%p\n", ((void *)((struct tstring_header_16 *)th)->buf));
      break;
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      size = ((struct tstring_header_32 *)th)->size;
      len = ((struct tstring_header_32 *)th)->len;
      xflags = ((struct tstring_header_32 *)th)->xflags;
      printf("TSTR_DEBUG:   is tstring_header_32\n");
      printf("TSTR_DEBUG:   th=%p\n", th);
      printf("TSTR_DEBUG:   xflags=%x\n", xflags);
      printf("TSTR_DEBUG:   size=%llu\n", (unsigned long long)size);
      printf("TSTR_DEBUG:   len=%llu\n", (unsigned long long)len);
      printf("TSTR_DEBUG:   header=%p\n", th);
      printf("TSTR_DEBUG:   buf=%p\n", ((void *)((struct tstring_header_32 *)th)->buf));
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      size = ((struct tstring_header_64 *)th)->size;
      len = ((struct tstring_header_64 *)th)->len;
      xflags = ((struct tstring_header_64 *)th)->xflags;
      printf("TSTR_DEBUG:   is tstring_header_64\n");
      printf("TSTR_DEBUG:   th=%p\n", th);
      printf("TSTR_DEBUG:   xflags=%x\n", xflags);
      printf("TSTR_DEBUG:   size=%llu\n", (unsigned long long)size);
      printf("TSTR_DEBUG:   len=%llu\n", (unsigned long long)len);
      printf("TSTR_DEBUG:   header=%p\n", th);
      printf("TSTR_DEBUG:   buf=%p\n", ((void *)((struct tstring_header_64 *)th)->buf));
      break;
#endif
    default:
      // something went wrong
      printf("TSTR_DEBUG:   tstring is broken (unknown type)\n");
      break;
  }
#endif
  fflush(stdout);
}
static void inline tstr_debugprinttstringn(tstring t, const char *t_name) {
  fflush(stdout);
  if (t_name) printf("TSTR_DEBUG: tstring %s\n", t_name);
  tstr_debugprinttstring(t);
}
#endif

#endif
/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Copy C string to tstring
 *
 * If s does not fit into t then a new tstring
 * will be dynamically created to hold s.
 * tstr_cpycn() avoids calling strlen() as it is
 * required for tstr_cpyc().
 * Thus it can be much faster. You should prefer
 * tstr_cpycn() if you know the len of s.
 *
 * @param t The tstring to be overwritten.
 * @param s The C string to be copied.
 * @param slen The length of the C string.
 * @return The overwritten tstring.
 */
tstring tstr_cpycn(tstring t, const char *s, size_t slen) {
  uint64_t cur_size;
  void *th;
  int is_readonly = 0;
  uint16_t xflags = 0;

#ifdef _TSTR_DEBUG
  if (slen < 64) {
    printf("TSTR_DEBUG: tstr_cpycn(%p, \"%s\", %lu)\n", (void *)t, s, slen); fflush(stdout);
  } else {
    printf("TSTR_DEBUG: tstr_cpycn(%p, <long string>, %lu)\n", (void *)t, slen); fflush(stdout);
  }
#endif
  if (__unlikely(t == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cpycn() error, t is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (__unlikely(s == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cpycn() error, s is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  //if (__unlikely(slen == 0)) { return t; }
  // in case of t being readonly simply allocate a new string
  //if (__unlikely(TSTR_IS_READONLY(t))) { return 0; }

  uint16_t cur_flags = ((uint16_t *)t)[-1];
  if (__unlikely(cur_flags & TSTR_READONLY)) { is_readonly = 1; }
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
  cur_size = ((struct tstring_header_32 *)th)->size;
  xflags = ((struct tstring_header_32 *)th)->xflags;
#else
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      cur_size = (cur_flags & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
      cur_size = ((struct tstring_header_16 *)th)->size;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
      cur_size = ((struct tstring_header_32 *)th)->size;
      xflags = ((struct tstring_header_32 *)th)->xflags;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
      cur_size = ((struct tstring_header_64 *)th)->size;
      xflags = ((struct tstring_header_64 *)th)->xflags;
      break;
#endif
    default:
      // something went wrong
#ifdef _TSTR_ERRNO
      tstr_errno = TSTR_ERROR_BROKENTSTRING;
#endif
      return 0;
      break;
  }
#endif

  // is current tstring big enough? remember the terminating 0
  // or do we need a new tstring because t is readonly?
  if (__unlikely((slen >= cur_size) || __unlikely(is_readonly))) {

    if (__likely(!is_readonly)) {
      // if it is not ro then check for noresize
      if (xflags & TSTR_NORESIZE) {
#ifdef _TSTR_DEBUG
        printf("TSTR_DEBUG: tstr_cpycn(): t IS noresize\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
        tstr_errno = TSTR_ERROR_NORESIZE;
#endif
        return 0;
      } else {
#ifdef _TSTR_DEBUG
        printf("TSTR_DEBUG: tstr_cpycn(): t is NOT noresize\n"); fflush(stdout);
#endif
      }
    }

#ifdef _TSTR_DEBUG
    if (is_readonly) {
      printf("TSTR_DEBUG: tstr_cpycn(): new rw tstring required\n"); fflush(stdout);
    } else {
      printf("TSTR_DEBUG: tstr_cpycn(): bigger tstring required\n"); fflush(stdout);
    }
#endif

    // need new or bigger tstring, calculate even more space
#ifdef _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
    size_t new_size = tstr_calculate_bigger_size(slen);
    if (__unlikely(new_size <= slen)) new_size = slen + 1; // dirty fallback
#else
    size_t new_size = slen + tstr_minimum_size;
#endif

    tstring t_new = tstr_new(new_size);
    if (__unlikely(t_new == 0)) return 0;

#ifdef _TSTR_DEBUG
    if (is_readonly) {
      printf("TSTR_DEBUG: tstr_cpycn(): new rw tstring created, size=%lu\n", tstr_size(t_new)); fflush(stdout);
    } else {
      printf("TSTR_DEBUG: tstr_cpycn(): bigger tstring created, size=%lu\n", tstr_size(t_new)); fflush(stdout);
    }
#endif

    // free old tstring
    if (__likely(!is_readonly)) {
      tstr_free(t);
    }

    // copy new data
    memcpy(t_new, s, slen);
    t_new[slen] = 0;

    // now set len
    if (__unlikely(tstr_setlen(t_new, slen) != TSTR_NOERROR)) {
      return 0;
    }

#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_cpycn(): return %p\n", (void *)t_new); fflush(stdout);
#endif
    return t_new;
  }

  // old tstring can still be used because its size is big enough

  // copy new data
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_cpycn(): memcpy(%p, %p, %lu)\n", (void *)t, (void *)s, slen); fflush(stdout);
#endif
  memcpy(t, s, slen);
  t[slen] = 0;

  // now set len
  if (__unlikely(tstr_setlen(t, slen) != TSTR_NOERROR)) {
    return 0;
  }
  
#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_cpycn(): return %p\n", (void *)t); fflush(stdout);
#endif
  return t;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

#include <stdlib.h>
// well, please be careful when using threads
#include <errno.h>

#ifndef _TSTR_DJB_ALLOC_BUFSIZE_KB
#define _TSTR_DJB_ALLOC_BUFSIZE_KB 0
#warning _TSTR_DJB_ALLOC_BUFSIZE_KB was not defined, setting it to 0
#endif

/**
 * PD by Daniel J. Bernstein:
 * alloc()/alloc_free()
 * modified/enhanced for tstring by Frank W. Bergmann
 */

#if _TSTR_DJB_ALLOC_BUFSIZE_KB != 0
#define ALIGNMENT 16 /* XXX: assuming that this alignment is enough */
#define SPACE 1024 * _TSTR_DJB_ALLOC_BUFSIZE_KB /* must be multiple of ALIGNMENT, condition matched by * 1024 */

typedef union { char irrelevant[ALIGNMENT]; double d; } aligned;
static aligned realspace[SPACE / ALIGNMENT];
#define space ((char *) realspace)
static unsigned int avail = SPACE; /* multiple of ALIGNMENT; 0<=avail<=SPACE */
#endif

/*@null@*//*@out@*/void *alloc(size_t n) {
  char *x;
#ifdef _TSTR_DEBUG
//    printf("TSTR_DEBUG: alloc(%lu)\n", n); fflush(stdout);
#endif
#if _TSTR_DJB_ALLOC_BUFSIZE_KB != 0
  n = ALIGNMENT + n - (n & (ALIGNMENT - 1)); /* XXX: could overflow */
  if (n <= avail) { avail -= n; return space + avail; }
#endif
#ifdef _TSTR_DEBUG
//  printf("TSTR_DEBUG: malloc(%lu)\n", n); fflush(stdout);
#endif
  x = malloc(n);
  /* if (!x) errno = error_nomem; */
  if (!x) errno = 127;
#ifdef _TSTR_DEBUG
//  printf("TSTR_DEBUG: malloc()=%p\n", (void *)x); fflush(stdout);
#endif
  return x;
}

void alloc_free(char *x) {
#if _TSTR_DJB_ALLOC_BUFSIZE_KB != 0
  if (x >= space)
    if (x < space + SPACE)
      return; /* XXX: assuming that pointers are flat */
#endif
#ifdef _TSTR_DEBUG
//  printf("TSTR_DEBUG: free(%p)\n", (void *)x); fflush(stdout);
#endif
  free(x);
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

#include <tstring.h>

/** @file
 * Return description of error.
 *
 * @param errnum The error number.
 * @return A description of the error.
 */
const char *tstr_strerror(int errnum) {
  switch (errnum) {
    case TSTR_NOERROR: return "No error."; break;
    case TSTR_ERROR_UNSPECIFIED: return "Unspecified error."; break;
    case TSTR_ERROR_NULLARGUMENT: return "NULL argument given."; break;
    case TSTR_ERROR_READONLY: return "tstring is marked read-only."; break;
    case TSTR_ERROR_NOALLOC: return "(Re-) allocating did fail."; break;
    case TSTR_ERROR_SIZETOOBIG: return "Size is too big."; break;
    case TSTR_ERROR_ILLEGALLEN: return "Illegal len value."; break;
    case TSTR_ERROR_BROKENTSTRING: return "Broken tstring."; break;
    case TSTR_ERROR_NORESIZE: return "tstring is marked noresize."; break;
    case TSTR_ERROR_NODUP: return "tstring is marked nodup."; break;
    case TSTR_ERROR_SIZETOOSMALL: return "Size is too small."; break;
    case TSTR_ERROR_FREEONSTATIC: return "Call tstr_free() on static memory."; break;
    default: return "unknown error code"; break;
  }
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

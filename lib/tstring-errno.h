#ifndef __LIBTSTRINGERRNO_H
#define __LIBTSTRINGERRNO_H

/** This is an usual non-threadsafe errno */
extern int tstr_errno;

#endif

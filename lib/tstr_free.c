#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Free a tstring.
 *
 * Call free() for a tstring if it was dynamically
 * allocated to release memory.
 * If t is a statically allocated tstring
 * then nothing will be done.
 *
 * @param t The tstring to be freed.
 * @return 0 is freeable and alloc_free() was called
 */
int tstr_free(tstring t) {
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
  size_t size;
#endif
  void *th = 0;

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_free(%p)\n", (void *)t); fflush(stdout);
#endif
  if (__unlikely(t == 0)) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return TSTR_ERROR_NULLARGUMENT;
  }
  tstr_setlen(t, 0); // ignore ro here

  uint16_t *p_flags = &((uint16_t *)t)[-1];
  uint16_t cur_flags = *p_flags;
  // modern systems don't allow use after free but set it anyway
  if (cur_flags & TSTR_FREEABLE) {
    *p_flags = cur_flags ^ TSTR_FREEABLE;
  }

  // mark it internally as "freed" by setting size to 0
#ifdef _TSTR_SINGLE_STRUCT_USAGE
  th = (struct tstring_header_32 *)(t - sizeof(struct tstring_header_32));
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
  size = ((struct tstring_header_32 *)th)->size;
#endif
  ((struct tstring_header_32 *)th)->size = 0;
#else
  uint16_t cur_type = TSTR_HEADERTYPE_MASK & cur_flags;
  switch(cur_type) {
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
    case TSTR_HEADERTYPE_6:
      th = (t - sizeof(struct tstring_header_6));
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
      size = (((uint16_t *)t)[-1] & TSTR_HEADERTYPE6_SIZEMASK) >> 6;
#endif
      ((uint16_t *)t)[-1] = 0;
      break;
    case TSTR_HEADERTYPE_16:
      th = (t - sizeof(struct tstring_header_16));
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
      size = ((struct tstring_header_16 *)th)->size;
#endif
      ((struct tstring_header_16 *)th)->size = 0;
      break;
#endif
    case TSTR_HEADERTYPE_32:
      th = (t - sizeof(struct tstring_header_32));
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
      size = ((struct tstring_header_32 *)th)->size;
#endif
      ((struct tstring_header_32 *)th)->size = 0;
      break;
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
    case TSTR_HEADERTYPE_64:
      th = (t - sizeof(struct tstring_header_64));
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
      size = ((struct tstring_header_64 *)th)->size;
#endif
      ((struct tstring_header_64 *)th)->size = 0;
      break;
#endif
    default:
#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
      size = 0;
#endif
      break;
  }
#endif

#ifdef _TSTR_USE_SECURE_ERASE_ON_FREE
  if (th != 0) erase_from_memory(th, t - (char *)th, t - (char *)th);
  if (size != 0) erase_from_memory(t, size, size);
#endif

  if (cur_flags & TSTR_FREEABLE) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_free(), alloc_free(%p)\n", (void *)th); fflush(stdout);
#endif
    alloc_free((char *)th);
    return 0;
  }

#ifdef _TSTR_DEBUG
  printf("TSTR_DEBUG: tstr_free() error, free on static memory (%p)\n", (void *)th); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
  tstr_errno = TSTR_ERROR_FREEONSTATIC;
#endif
  return TSTR_ERROR_FREEONSTATIC;
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

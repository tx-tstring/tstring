#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <stdio.h>

int tstr_is_freeable(tstring t) {
  uint16_t *p_flags = &((uint16_t *)t)[-1];
  uint16_t cur_flags = *p_flags;
  if (cur_flags & TSTR_FREEABLE) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_is_freeable(%p) is freeable\n", (void *)t); fflush(stdout);
#endif
    return 0;
  } else {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_is_freeable(%p) is NOT freeable\n", (void *)t); fflush(stdout);
#endif
    return 1;
  }
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

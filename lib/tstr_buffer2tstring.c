#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Initialize buffer as tstring
 *
 * Initialize an already allocated buffer as tstring.
 * If size of buf can be handled as tstring then tstring will be returned.
 * <br>
 * <br>
 *
 * Total size can be (using tstring_header_6 as example):<br>
 * - 2 bytes flags<br>
 * - 0..63 bytes actual data<br>
 * - including terminating 0<br>
 * = 65 bytes for header6
 *
 * @param buf The buffer to be initialized.
 * @param size The size of buf.
 * @param freeable A value != 0 will mark the tstring as dynamically allocated.
 * @return tstring on success or NULL on error
 */
tstring tstr_buffer2tstring(char *buf, size_t size, uint16_t freeable) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_buffer2tstring(%p, %lu, %hu)\n", (void *)buf, size, freeable); fflush(stdout);
#endif
  if (buf == 0) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  if (size < 14) {
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_SIZETOOSMALL;
#endif
    return 0;
  }
#ifdef TSTR_ALLOW_16_BIT_ALIGNMENT
  if (size <= (TSTR_HEADERTYPE6_LENMASK + sizeof(struct tstring_header_6))) {
    return tstr_init6(buf, size, freeable);
  }
  //if (size <= ((size_t)UINT16_MAX) + sizeof(struct tstring_header_16)) {
  if (size <= ((size_t)UINT16_MAX)) {
    return tstr_init16(buf, size, freeable);
  }
#endif
  // won't work on archs smaller than 32 bit
  if (size <= ((size_t)UINT32_MAX)) {
    return tstr_init32(buf, size, freeable);
  }
#ifdef TSTR_ALLOW_EXABYTE_STRINGS
  return tstr_init64(buf, size, freeable);
#else
#ifdef _TSTR_ERRNO
  tstr_errno = TSTR_ERROR_SIZETOOBIG;
#endif
  return 0;
#endif
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

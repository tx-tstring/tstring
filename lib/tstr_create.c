#define _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H
#include <tstring-private.h>
#undef _TSTR_ALLOW_INCLUDING_OF_LIBTSTRINGPRIVATE_H

/** @file
 * Create tstring from C string
 *
 * If s is 0 then no tstring will be created.
 * You should prefer tstr_createn()
 * if you know the len of s.
 *
 * @param s The C string from which the tstring will be created.
 * @return The new tstring. 0 on error.
 */
tstring tstr_create(const char *s) {
  if (__unlikely(s == 0)) {
#ifdef _TSTR_DEBUG
    printf("TSTR_DEBUG: tstr_create() error, s is 0\n"); fflush(stdout);
#endif
#ifdef _TSTR_ERRNO
    tstr_errno = TSTR_ERROR_NULLARGUMENT;
#endif
    return 0;
  }
  return tstr_createn(s, strlen(s));
}

/* use modeline modelines=1 in vimrc */
/* vim: set ft=c sts=2 sw=2 ts=2 ai et: */

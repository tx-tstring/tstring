.TH tstring 3 "January 2024" "tstring" "tstring C string library"

.SH NAME
.B tstring
-- C string library

.SH LIBRARY
tstring, a C string library (-ltstring)

.SH SYNOPSIS
.B #include <tstring.h>
.br
[see
.I tstring.h
for full description]

.SH DESCRIPTION
tstring is a C string library with these primary goals:
.IP * 3
.B security
(no buffer overflows, R/O strings, secure free memory, many unit tests)
.IP *
.B suitable for embedded usage
(lightweight, small structs, default small dynamic grow factor)
.IP *
.B compatible to C string functions
(greetings to the awesome SDS strings!)
.IP *
.B fast
(less strlen(), less copies)
.PP
.PP
.PP
Features in detail:
.IP * 3
tstring is compatible to 'char *' of classic C string functions - no conversion required
.IP *
size and length are handled to avoid buffer overflows
.IP *
automated growth of strings
.IP *
lightweight
.IP *
able to secure erase memory on free() and new()
.IP *
able to store binary data including '\\0'
.IP *
supports static and dynamic allocation
.IP *
supports read-only strings
.IP *
data is stored CPU cache friendly within struct of tstring (no pointer)
.IP *
ANSI C, compatible with many environments like e.g. POSIX, Linux, *BSD
.IP *
widely configurable with self explaining tstring-config.h
.IP *
configurable for different struct header sizes and alignments
.IP *
suitable for exabyte strings or even embedded usage
.IP *
unit tested
.IP *
comes with man pages
.PP
.PP
.PP
.I Note:
The library uses the standard OS functions memchr(),
memcpy(), memcmp(), memmem() and strlen().
No changes are done at this level.
This also means that if you use special implementations
of these functions that use e.g. SSE2 or AVX2 then
.B tstring
will benefit from this.

.SH FUNCTIONS
.I tstring
.B tstr_new
(
.I size_t size
);
.I // allocate tstring
.br
.I tstring
.B tstr_create
(
.I const char *s
);
.I // create tstring from C string
.br
.I tstring
.B tstr_createn
(
.I const char *s
,
.I size_t slen
);
.I // create tstring from C string
.br
.I tstring
.B tstr_init
(
.I char *buf
,
.I size_t size
);
.I // initialize dynamically allocated buffer as tstring
.br
.I tstring
.B tstr_init_static
(
.I char *buf
,
.I size_t size
);
.I // initialize static buffer as tstring
.br
.I tstring
.B tstr_buffer2tstring
(
.I char *buf
,
.I size_t size
,
.I uint16_t freeable
);
.I // initialize buffer as tstring
.br

.I tstring
.B tstr_cpy
(
.I tstring t
,
.I const tstring s
);
.I // copy tstring
.br
.I tstring
.B tstr_cpyc
(
.I tstring t
,
.I const char *s
);
.I // copy string
.br
.I tstring
.B tstr_cpycn
(
.I tstring t
,
.I const char *s
,
.I size_t slen
);
.I // copy string
.br

.I tstring
.B tstr_cat
(
.I tstring t
,
.I const tstring s
);
.I // append tstring
.br
.I tstring
.B tstr_catc
(
.I tstring t
,
.I const char *s
);
.I // append string
.br
.I tstring
.B tstr_catcn
(
.I tstring t
,
.I const char *s
,
.I size_t slen
);
.I // append string
.br

.I char *
.B tstr_chr
(
.I tstring t
,
.I int c
);
.I // locate char in tstring
.br
.I char *
.B tstr_tstr
(
.I tstring t
,
.I tstring s
);
.I // locate a tstring in tstring
.br
.I char *
.B tstr_str
(
.I tstring t
,
.I const char *needle
);
.I // locate a substring in tstring
.br
.I char *
.B tstr_mem
(
.I tstring t
,
.I const char *little
,
.I size_t little_len
);
.I // locate a byte substring in tstring
.br

.I tstring
.B tstr_resize
(
.I tstring t
,
.I size_t newsize
);
.I // resize tstring
.br
.I tstring
.B tstr_dup
(
.I tstring t
);
.I // duplicate tstring
.br

.I size_t
.B tstr_len
(
.I tstring t
);
.I // get length of string data
.br
.I size_t
.B tstr_size
(
.I tstring t
);
.I // get storage size of tstring
.br
.I int
.B tstr_setro
(
.I tstring t
);
.I // make tstring read-only
.br
.I int
.B tstr_setrw
(
.I tstring t
);
.I // make tstring writeable
.br
.I int
.B tstr_setnoresize
(
.I tstring t
);
.I // make tstring to non-resizeable
.br
.I int
.B tstr_setresize
(
.I tstring t
);
.I // make tstring to resizeable
.br
.I int
.B tstr_setlen
(
.I tstring t
,
.I size_t new_len
);
.I // set tstring len
.br

.I int
.B tstr_free
(
.I tstring t
);
.I // free/destroy tstring, release memory
.br

.I int
.B tstr_cmp
(
.I tstring t
,
.I tstring s
);
.I // compare two tstrings
.br
.I tstring
.B tstr_substr
(
.I tstring t
,
.I size_t s
,
.I size_t e
);
.I // create a substring from a tstring

.PP
The
.B tstring
library supports binary data.
But some functions take C strings as input data
and hence support string data only.
.br
List of functions that do
.I not
support binary data:
.IP * 3
.B tstr_catc()
uses strlen() internally
.IP *
.B tstr_cpyc()
uses strlen() internally
.IP *
.B tstr_create()
uses strlen() internally
.IP *
.B tstr_str()
uses strstr() internally

.SH EXAMPLES
Example build time configuration for embedded usage:
.RS
$ echo "#define _TSTR_USE_EMBEDDED_CONFIGURATION" >lib/tstring-config-local.h
.br
$ make
.RE
.PP
Example build time configuration for security:
.RS
$ echo "#define _TSTR_USE_SECURE_ERASE_ON_FREE" >lib/tstring-config-local.h
.br
$ echo "#define _TSTR_USE_SECURE_ERASE_ON_INIT" >>lib/tstring-config-local.h
.br
$ make
.RE
.PP
Example build time configuration for somewhat more speed:
.RS
$ echo "#undef _TSTR_USE_SECURE_ERASE_ON_FREE" >lib/tstring-config-local.h
.br
$ echo "#undef _TSTR_USE_SECURE_ERASE_ON_INIT" >>lib/tstring-config-local.h
.br
$ echo "#define _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS" >>lib/tstring-config-local.h
.br
$ echo "#undef TSTR_ALLOW_16_BIT_ALIGNMENT" >>lib/tstring-config-local.h
.br
$ make
.RE
.PP
Simple "cat" example:
.RS
char buf1[SIZE1];
.br
// initialize static data storage "buf1" as tstring
.br
tstring ts = tstr_init_static(buf1, SIZE1);
.br
// copy some data into tstring
.br
tstr_cpycn(ts1, "#", 1);
.br
// cat some data to ts1, ts_new will be == ts if STRING1 did fit
.br
ts_new = tstr_catcn(ts1, STRING1, sizeof(STRING1));
.br
// simply print the tstring
.br
puts(ts_new); // no "conversion" required
.RE
.br

.SH SEE ALSO
tstr_len(3),
tstr_size(3),
tstr_cpyc(3),
tstr_cpycn(3),
strcpy(3),
tstr_catc(3),
tstr_catcn(3),
strcat(3),
tstr_dup(3),
strdup(3),
tstr_create(3),
tstr_init(3),
tstr_init_static(3),
tstr_buffer2tstring(3),
tstr_new(3),
tstr_free(3),
tstr_setlen(3),
tstr_setro(3),
tstr_setrw(3),
tstr_chr(3),
tstr_str(3),
tstr_mem(3),
tstr_tstr(3),
tstr_substr(3),
tstr_cmp(3)
.br
http://www.tuxad.com

.SH STANDARDS
tstring functions
do not conform to
any standard.
Some function names are loosely related to original C string library functions.

.SH AUTHORS AND LICENSE
Frank W. Bergmann
http://www.tuxad.com
.br
BSD 2-Clause License


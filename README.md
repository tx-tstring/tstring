# tstring - simple C string library

tstring is a C string library with these primary goals:

   * **security** (no buffer overflows, R/O strings, secure free memory, many unit tests)
   * **suitable for embedded usage** (lightweight, small structs, default small dynamic grow factor)
   * **compatible to C string functions** (greetings to the awesome SDS strings!)
   * be somewhat **fast** (less strlen(), less copies)

1. [Features in detail](#features-in-detail)
2. [Requirements](#requirements)
3. [Usage](#usage)
    1. [Creation of a tstring](#creation-of-a-tstring)
    2. [Assigning data to a tstring](#assigning-data-to-a-tstring)
    3. [cat / concatenation](#cat-concatenation)
    4. [Searching](#searching)
    5. [Resizing tstrings](#resizing-tstrings)
    6. [tstring properties](#tstring-properties)
    7. [free, release memory](#free-release-memory)
4. [Lib internals](#lib-internals)
5. [Building](#building)
    1. [Library build](#library-build)
    2. [Building your code against tstring lib](#building-your-code-against-tstring-lib)
6. [Comparison to other C string](#comparison-to-other-c-string-libs)
    1. [glib](#glib)
    2. [bstring](#bstring)
    3. [SDS](#sds)
    4. [Disadvantages of tstring](#disadvantages-of-tstring)

## Features in detail

   * tstring is compatible to 'char \*' of classic C string functions - no conversion required
   * direct access of chars via `tstring[]`
   * size and length are handled to avoid buffer overflows
   * automated growth of strings
   * lightweight
   * able to secure erase memory on free() and new()
   * able to store binary data including '\\0'
   * supports static and dynamic allocation
   * supports read-only strings
   * limitation from unqualified sources supported
   * data is stored CPU cache friendly within struct of tstring (no pointer)
   * ANSI C, compatible with many Unix systems
   * widely configurable with self explaining tstring-config.h
   * configurable for different struct header sizes and alignments
   * suitable for exabyte strings or even embedded usage
   * unit tested
   * comes with man pages
   * debugging support

## Requirements

   * Linux, GNU, FreeBSD, OpenBSD, NetBSD, macOS etc.
   * gcc >= 4, clang (or compatible)
   * make

## Usage

A full list of functions is given in the man page `tstring(3)`.

### Creation of a tstring

```c
tstring tstr_new(size_t size); // allocate tstring
tstring tstr_create(const char *s);// create tstring from C string
tstring tstr_createn(const char *s, size_t slen); // create tstring from C string
tstring tstr_init(char *buf, size_t size); // initialize dynamically allocated buffer as tstring
tstring tstr_init_static(char *buf, size_t size); // initialize static buffer as tstring
tstring tstr_buffer2tstring(char *buf, size_t size, uint16_t freeable); // initialize buffer as tstring
```


An empty tstring with a specified size is created this way:

```c
tstring ts;
ts = tstr_new(256);
// ...
// some code
// ...
tstr_free(ts);
```

This will dynamically create a new tstring.
Creation of a tstring in static space is done like this:

```c
tstring ts;
char buf[256];
ts = tstr_init_static(buf, 256);
// no need to call tstr_free()
```

Because the whole tstring struct including header and the actual
string data is stored in this buffer you have less space for string data
than 256. Depending on you configuration you can store up to 250 bytes or
244 bytes of string data into this tstring.

By using the macro `TSTR_SIZE()` you can add more bytes to hold the header:

```c
tstring ts;
char buf[TSTR_SIZE(256)];
ts = tstr_init_static(buf, sizeof(buf));
// no need to call tstr_free()
```

Here you will have full 256 bytes free for string data.

Creating a tstring from a C string:

```c
tstring ts1, ts2;
ts1 = tstr_create("Hello");
ts2 = tstr_createn("abc", 3); // faster
printf("%s\n", ts1);
printf("%s\n", ts2);
printf("%c\n", ts2[2]); // 'c'
tstr_free(ts1);
tstr_free(ts2);
```

The function `tstr_create()` is somewhat slower because it has to call `strlen()`.
Hence if you know the length of a C string then better use `tstr_createn()`.

Here you can also see one advantage of a tstring: It can be directly used as argument
in standard C functions.

WARNING: You MUST NOT modify a tstring with standard C functions because that will break
a tstring internally. To modify a tstring you should use tstring functions only because
this will also protect you from buffer overflows.

### Assigning data to a tstring

```c
tstring tstr_cpy(tstring t, const tstring s); // copy tstring
tstring tstr_cpyc(tstring t, const char *s); // copy string
tstring tstr_cpycn(tstring t, const char *s, size_t slen); // copy string
```

Like the standard C "cpy" functions there are also related tstring functions:

```c
tstring ts1, ts2;
char buf1[256];
char buf2[256];
ts1 = tstr_init_static(buf1, 256);
// copy "Hello" into ts1
ts1 = tstr_cpyc(ts1, "Hello");
// copy "abcd" into ts2
ts2 = tstr_cpycn(ts2, "abcd", 4);
// copy ts1 into ts2
ts2 = tstr_cpy(ts2, ts1);
// no need to call tstr_free()
```

### `cat` / concatenation

```c
tstring tstr_cat(tstring t, const tstring s); // append tstring
tstring tstr_catc(tstring t, const char *s); // append string
tstring tstr_catcn(tstring t, const char *s, size_t slen); // append string
```

Like the standard C "cat" functions there are also related tstring functions:

```c
tstring ts1, ts2;
char buf1[256];
char buf2[256];
ts1 = tstr_init_static(buf1, 256);
ts2 = tstr_init_static(buf2, 256);
// copy "Hello" to ts1 via cat
ts1 = tstr_catc(ts1, "Hello");
// copy "abcd" to ts2 via cat
ts2 = tstr_catcn(ts2, " world", 6);
// cat ts2 to ts1
ts1 = tstr_cat(ts1, ts2);
// prints "Hello world"
printf("%s\n", ts1);
// prints "l"
printf("%c\n", ts1[2]);
// no need to call tstr_free()
```

As you see a tstring can be directly used in C functions without any
"conversion". You can even access chars directly via `tstring[]`.

### Searching

```c
char *tstr_chr(tstring t, int c); // locate char in tstring
char *tstr_tstr(tstring t, tstring s); // locate a tstring in tstring
char *tstr_str(tstring t, const char *needle); // locate a substring in tstring
char *tstr_mem(tstring t, const char *little, size_t little_len); // locate a byte substring in tstring
```

The prototypes should be self-explanatory. Details are given in the man pages.

### Resizing tstrings

```c
tstring tstr_resize(tstring t, size_t new_size); // resize tstring
tstring tstr_dup(tstring t); // duplicate tstring
```

If any modification of a tstring requires a bigger tstring then
a new tstring will be created and returned. This is the automated
growth of tstrings.

You can't directly modify the size of a tstring. Here "size" means the
tstring header and the space reserved for string data.
See [Lib internals](#lib-internals) below.

Nevertheless there is a dedicated call although its usage is not
recommended:

```c
char buf[50];
// initialize static data storage "buf" as tstring
tstring ts = tstr_init_static(buf, 50);
// ... (some code)
// size is bigger, new tstring allocated
ts = tstr_resize(ts, 100);
// ... (some code)
// because ts is now dynamically allocated we need free
tstr_free(ts);
```

For more details please check the man page `tstr_resize(3)`.
The function `tstr_dup(3)` does also a resize by setting size = len.

### tstring properties

```c
size_t tstr_len(tstring p); // get length of string data
size_t tstr_size(tstring p); // get storage size of tstring
int tstr_setro(tstring t); // make tstring read-only
int tstr_setrw(tstring t); // make tstring writeable
int tstr_setnoresize(tstring t); // set tstring to non-resizeable
int tstr_setresize(tstring t); // set tstring to resizeable
```

The prototypes should be almost self-explanatory. Details are given in the man pages.

Setting a tstring to "noresize" / non-resizeable means that automated growth is
limited. The tstring can grow until its storage size is reached (len == size)
but not beyond. No new/bigger tstring will be allocated.

The idea is coming from the bstring library where it is called "limitation from
unqualified sources". (Wasn't there recently a software which did run into an
overflow because someone did send a 4 GB HTTP header?)

### `free`, release memory

```c
int tstr_free(tstring t); // free/destroy tstring, release memory
```

Although static allocation is well supported you will usually
get a lot of dynamic allocations. Because every time an
automated growth of a tstring will happen it will be
dynamically allocated - unless it did fit into a SMALL
internal static buffer (thanks to DJB).

There's no need to track if a tstring is statically or dynamically
allocated because this is already done internally by the lib.
Actually it is safe to call `tstr_free()` even on a statically
allocated tstring.

NOTE: If you did set a tstring to read-only then this won't
protect it from being destroyed by `tstr_free()`!


## Lib internals

A VERY simplified representation of tstring's internal struct and the tstring
typedef:

```c
struct tstring_header {
  /** the actual length of string data */
  int len;
  /** the allocated size of the buffer */
  int size;
  /** misc flags like e.g. read only, freeable, header type */
  int flags;
  /** buffer holds actual string data */
  char buf[];
};

typedef char* tstring;
```

If you defined a variable like this:

```c
tstring ts;
```

Then `ts` actually points to the `buf` member of the struct. That's why a
tstring is compatible to C library string functions (again greetings to the
awesome SDS strings).

Actually the library can contain up to four different structs
depending on the build time configuration (e.g. embedded usage):

   * `tstring_header_6` (up to 64 bytes)
   * `tstring_header_16` (up to 64 KiB)
   * `tstring_header_32` (up to 4 GiB)
   * `tstring_header_64` (up to 8 EiB)

While `tstring_header_32` does always exist the other three are library build
time configurable. Unfortunately this means that an embedded system must be 32
bit capable. But you can save memory by using `tstring_header_6` and
`tstring_header_16` and a low growth factor.

Most other enhanced C string libs don't use an array like `char buf[]` but a
pointer like `char *buf`. This has the big advantage that if you a function like
e.g. `xxxlib_cat(xxx, "test")` it could happen that a automated growth took
place and then the internal pointer `char *buf` will point to a different
location but your struct pointer will **always** be the same.

The tstring lib is different. It does NOT have external data but includes the
storage buffer in the struct. This means that the tstring pointer might change
on automated growth and hence you have to "assign it back" because the struct
pointer did change:

```c
ts = tstr_catc(ts, "some string"); // instead of just xxx_cat(xxx, "some string")
```

By doing that you will get in case of heavy string usage a somewhat less
fragmented memory and the actual data is near by the struct data (which is
somewhat more CPU cache friendly).


## Building

In the most simple case you just need to call `make` to build the lib and to
include `tstring.h` to build your code against it.

### Library build

#### Compile

   * Optional: Edit lib/tstring-config.h or simply add your own lib/tstring-config-local.h
   * make
   * make check
   * make install

This will install the library in `/usr/local`. A different path can be used like this:

```
make prefix=/home/myuser install
```

#### Compile time configuration

The default and always existing tstring header / struct
is tstring\_header\_32 with 32 bit addressing (up to 4 GiB).
The header is 32 bit aligned. It is also possible to use 16
bit alignment and get two additional headers for smaller
tstrings and lower memory usage. On 64 bit systems it is also
possible to use 64 bit tstrings.

Defines:

   * TSTR\_ALLOW\_16\_BIT\_ALIGNMENT: use 16 bit header alignment and enable tstring\_header\_6 and tstring\_header\_16
   * TSTR\_ALLOW\_EXABYTE\_STRINGS: enable 64 bit tstrings (actually 63 bit)
   * On build time "visible": \_TSTR\_USE\_SECURE\_ERASE\_ON\_FREE: enable secure erasing of tstrings before freeing
   * On build time "visible": \_TSTR\_USE\_SECURE\_ERASE\_ON\_INIT: enable secure erasing of tstrings after initializing (seldom required)
   * On build time "visible": \_TSTR\_ADD\_EXTRA\_SPACE\_ON\_NEW\_TSTRINGS: When creating new tstrings e.g. from C strings then add extra free space
   * On build time "visible": \_TSTR\_USE\_EMBEDDED\_CONFIGURATION (like a "meta define"): disable TSTR\_USE\_SECURE\_ERASE\*, disable TSTR\_ADD\_EXTRA\_SPACE\_ON\_NEW\_TSTRINGS, enable TSTR\_ALLOW\_16\_BIT\_ALIGNMENT, disable TSTR\_ALLOW\_EXABYTE\_STRINGS

The configuration can be stored in `lib/tstring-config-local.h`.

Example build time configuration for embedded usage:

```
$ echo "#define _TSTR_USE_EMBEDDED_CONFIGURATION" >lib/tstring-config-local.h
$ make
```

Example build time configuration for security:

```
$ echo "#define _TSTR_USE_SECURE_ERASE_ON_FREE" >lib/tstring-config-local.h
$ echo "#define _TSTR_USE_SECURE_ERASE_ON_INIT" >>lib/tstring-config-local.h
$ make
```

Example build time configuration for somewhat more speed:

```
$ echo "#undef _TSTR_USE_SECURE_ERASE_ON_FREE" >lib/tstring-config-local.h
$ echo "#undef _TSTR_USE_SECURE_ERASE_ON_INIT" >>lib/tstring-config-local.h
$ echo "#define _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS" >>lib/tstring-config-local.h
$ echo "#undef TSTR_ALLOW_16_BIT_ALIGNMENT" >>lib/tstring-config-local.h
$ make
```

For debugging set the macro `_TSTR_DEBUG`

```c
make EXTRACFLAGS="-g -D_TSTR_DEBUG"
```

If you build your code against this build then you will get a lot of useful
details which show you what is actually going on.

In addition there are two debug functions which print all details of a tstring:

```c
static void inline tstr_debugprinttstring(tstring t);
static void inline tstr_debugprinttstringn(tstring t, const char *t_name);
```

#### Example Binary size

Two example sizes of the tstring lib compiled with `dietlibc` on Linux:

    $ ls -l libtstring.a 
    -rw-r--r--. 1 fb  fb     46922 Jan 20 20:09 libtstring.a

And on macOS Catalina:

    $ ls -l libtstring.a 
    -rw-r--r--  1 fb  staff  19880 Jan 20 20:04 libtstring.a


### Building your code against tstring lib

With the default installation in `/usr/local` you can compile your program like that:

```
gcc -I/usr/local/include -L/usr/local/lib -ltstring -o myprog myprog.c
```

You can remember "ltstring" as "lite string" or "lightweight string".

## Comparison to other C string libs

Some years ago I was somewhat unhappy about the situation and made a comparison
of some well designed C string libs. It's in the doc dir:
[README.string-libs.txt](doc/README.string-libs.txt)

Also worth to mention is an awesome comparison of string libs by James Antill:
[http://www.and.org/vstr/comparison](http://www.and.org/vstr/comparison).
It's not just about C string libs but also C++ string libs.

Because there seem to be hundreds of C string libs I just wanna mention some of
the IMHO best libs available:

   * glib
   * Better String Library (bstring)
   * SDS strings
   * SafeStr
   * Ustr micro string library
   * rapidstring

Not listed are string libs which are not available as standalone libs but are
included in bigger software packages like e.g. Wietse Venema's string
implementation.

All this eventually caused the journey to create even another C string library
which is strongly influenced by the design of the SDS strings but also by some
other string libs.

### glib

One of the "standard libs", widely used.

   * simple struct
   * automated growth
   * binary data capable
   * gsize platform dependent
   * pointer to actual data

### bstring

It's an awesome and very well designed lib and I'm actually wondering why it is
not very widely used.

   * simple struct
   * automated growth
   * binary data capable
   * pointer to actual data
   * int len platform dependent
   * read-only support
   * unqualified sources lead to RO strings
   * optional maximum length
   * no changes since 2015

### SDS

Just check which companies are using SDS. Did I mention that it is awesome?

   * SDS strings can be used as arguments for C library functions
   * (still) simple struct
   * automated growth
   * binary data capable
   * actual data is part of struct
   * alignment not configurable

As tstring is somewhat based on the ideas of SDS you might also think about
reading their docs.

While tstring and SDS use negative offsets for the header the common father
could be maybe SafeStr.

### Disadvantages of tstring

After talking about other libs I want to point out some disadvantages or at
least differences of tstring compared to other libs:

   * while embedded systems are also targeted they must have an >= 32 bit architecture
   * no data sharing or indexing support (James Antill)
   * address of a tstring might be changed, requires assigning back
   * no printf or even fmt support
   * no reference counter (SafeStr)
   * no cookie or detection of external mods (SafeStr)
   * missing a lot of useful functions/features
   * security can still be increased a lot (see e.g. `tstr_cmp(3)`)

Well, some things like "data sharing" or "reference counter" are things which
are also not supported by most other libs but we should still keep an eye on it
and write it down here. Implementing a reference counter will surely never be
implemented in tstring itself but maybe in an extended library like e.g.
txstring. And sharing support is a great feature which might be implemented in
future.


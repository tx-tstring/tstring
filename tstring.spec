%global _prefix /usr/local
%global debug_package %{nil}

Name: tstring
Version: 0.20240122
Release: 1%{?dist}
Summary: a C string library
License: BSD-2-Clause
URL: https://www.tuxad.com
Source0: %{name}-%{version}.tar.gz

BuildRequires: gcc
BuildRequires: make
BuildRequires: doxygen

%description
tstring is a C string library with these primary goals:

* security (no buffer overflows, R/O strings, secure free memory, many unit tests)
* suitable for embedded usage (lightweight, small structs, default small dynamic grow factor)
* compatible to C string functions (greetings to the awesome SDS strings)
* be somewhat fast (less strlen(), less copies)

Features in detail:

* tstring is compatible to 'char *' of classic C string functions - no conversion required
* size and length are handled to avoid buffer overflows
* automated growth of strings
* lightweight
* able to secure erase memory on free() and new()
* able to store binary data including '\\0'
* supports static and dynamic allocation
* supports read-only strings
* data is stored CPU cache friendly within struct of tstring (no pointer)
* ANSI C, compatible with many environments like e.g. POSIX, Linux, *BSD
* widely configurable with self explaining tstring-config.h
* configurable for different struct header sizes and alignments
* suitable for exabyte strings or even embedded usage
* unit tested
* comes with man pages

%prep
%setup -q

%build
cat >lib/tstring-config-local.h <<EOF
#undef _TSTR_USE_SECURE_ERASE_ON_FREE
#undef _TSTR_USE_SECURE_ERASE_ON_INIT
#define _TSTR_ADD_EXTRA_SPACE_ON_NEW_TSTRINGS
#undef TSTR_ALLOW_16_BIT_ALIGNMENT
#undef TSTR_ALLOW_EXABYTE_STRINGS
#undef _TSTR_DJB_ALLOC_BUFSIZE_KB
#define _TSTR_DJB_ALLOC_BUFSIZE_KB 500
#define TSTR_REALLOC_ALLOC_LIMIT 1024*1024*1024
EOF
make EXTRACFLAGS= lib check

%install
[ -d  ${RPM_BUILD_ROOT} -a "${RPM_BUILD_ROOT}" != "/" ] && rm -rf  ${RPM_BUILD_ROOT}
make EXTRACFLAGS= DESTDIR=${RPM_BUILD_ROOT} install

%files
%doc README.md LICENSE.md
%{_prefix}/lib/libtstring.a
%{_includedir}/tstring.h
%{_mandir}/man3/tstring.3.gz
%{_mandir}/man3/tstr_buffer2tstring.3.gz
%{_mandir}/man3/tstr_cat.3.gz
%{_mandir}/man3/tstr_catc.3.gz
%{_mandir}/man3/tstr_catcn.3.gz
%{_mandir}/man3/tstr_chr.3.gz
%{_mandir}/man3/tstr_cmp.3.gz
%{_mandir}/man3/tstr_cpy.3.gz
%{_mandir}/man3/tstr_cpyc.3.gz
%{_mandir}/man3/tstr_cpycn.3.gz
%{_mandir}/man3/tstr_dup.3.gz
%{_mandir}/man3/tstr_free.3.gz
%{_mandir}/man3/tstr_create.3.gz
%{_mandir}/man3/tstr_createn.3.gz
%{_mandir}/man3/tstr_init.3.gz
%{_mandir}/man3/tstr_init_static.3.gz
%{_mandir}/man3/tstr_len.3.gz
%{_mandir}/man3/tstr_mem.3.gz
%{_mandir}/man3/tstr_new.3.gz
%{_mandir}/man3/tstr_resize.3.gz
%{_mandir}/man3/tstr_setlen.3.gz
%{_mandir}/man3/tstr_setro.3.gz
%{_mandir}/man3/tstr_setrw.3.gz
%{_mandir}/man3/tstr_size.3.gz
%{_mandir}/man3/tstr_str.3.gz
%{_mandir}/man3/tstr_substr.3.gz
%{_mandir}/man3/tstr_tstr.3.gz
%{_mandir}/man3/tstr_setnoresize.3.gz
%{_mandir}/man3/tstr_setresize.3.gz

%changelog
* Mon Jan 22 2024 Frank Bergmann <something@tuxad.com> - 0.20240122-1
- enhance docs
- add noresize feature
* Sat Jan 20 2024 Frank Bergmann <something@tuxad.com> - 0.20240120-1
- enhance docs a lot
- rename some functions
- add tstr_resize()
- enhance unit tests
- enhance Makefile
- some fixes
* Sat May 13 2023 Frank Bergmann <something@tuxad.com> - 0.20230513-1
- enhance docs
* Fri May 12 2023 Frank Bergmann <something@tuxad.com> - 0.20230512-1
- initial release

